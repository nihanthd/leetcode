module bitbucket.org/nihanthd/leetcode

go 1.13

require (
	github.com/aws/aws-sdk-go v1.25.14 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.2.0 // indirect
	go.uber.org/zap v1.10.0 // indirect
)
