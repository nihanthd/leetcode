package main

import (
	"fmt"

	"bitbucket.org/nihanthd/leetcode/src/design"
)

func main() {
	stack := &design.Stack{}
	stack.Push(3)
	stack.Push(5)
	stack.Push(8)
	fmt.Println("----", stack.A)
	fmt.Println(stack.Peek())
	fmt.Println(stack.Pop())
	fmt.Println(stack.Pop())
	fmt.Println(stack.Pop())

	minStack := &design.SpecialStack{}
	minStack.Push(4)
	minStack.Push(5)
	fmt.Println("-----", minStack)
	fmt.Println(minStack.GetMin())
	minStack.Push(3)
	fmt.Println("-----", minStack)
	fmt.Println(minStack.GetMin())
	minStack.Pop()
	fmt.Println(minStack.GetMin())
	fmt.Println("-----", minStack)
	minStack.Pop()
	fmt.Println(minStack.GetMin())
	fmt.Println("-----", minStack)
}
