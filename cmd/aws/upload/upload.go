package upload

import (
	"flag"

	"go.uber.org/zap"

	"bitbucket.org/nihanthd/leetcode/aws"
)

func main() {
	logger, _ := zap.NewDevelopment()
	awsKey := flag.String("key", "aws-key", "Key")
	awsSecret := flag.String("secret", "aws-secret", "Secret")
	awsBucket := flag.String("bucket", "aws-bucket", "Bucket")
	path := flag.String("path", "path", "Path")
	flag.Parse()

	logger.Debug("path", zap.String("path", *path))
	aws.UploadToS3(*awsBucket, *awsKey, *awsSecret, *path, logger)
}
