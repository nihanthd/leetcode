package download

import (
	"flag"

	"bitbucket.org/nihanthd/leetcode/aws"
	"go.uber.org/zap"
)

func main() {
	logger, _ := zap.NewDevelopment()
	awsKey := flag.String("key", "aws-key", "Key")
	awsSecret := flag.String("secret", "aws-secret", "Secret")
	awsBucket := flag.String("bucket", "aws-bucket", "Bucket")
	awsRegion := flag.String("region", "aws-region", "Region")
	path := flag.String("path", "path", "Path")
	flag.Parse()
	logger.Debug("path", zap.String("path", *path))
	aws.DownloadFromS3(*awsBucket, *awsKey, *awsSecret, *awsRegion, *path, logger)
}
