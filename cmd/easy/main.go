package main

import (
	"bitbucket.org/nihanthd/leetcode/src/easy"
)

func main() {
	//easy.AddTwoNumber()
	//easy.LongestCommonPrefix()
	//easy.NewMaxSubArraySum()
	//easy.FindPairs()
	//root := &easy.TreeNode{
	//	Val:  1,
	//	Left: nil,
	//	Right: &easy.TreeNode{
	//		Val: 3,
	//		Left: &easy.TreeNode{
	//			Val:   2,
	//			Left:  nil,
	//			Right: nil,
	//		},
	//		Right: nil,
	//	},
	//}
	//
	//easy.InorderTraversal(root)
	//
	//easy.GetMinimumDifference()
	//easy.TwoSum()
	//easy.MinimumDepthOfABinaryTree()
	//easy.MaximumDepthOfBinaryTree()
	//easy.BalancedBinaryTree()
	//easy.KeyboardRow()
	//bit.PowerOfTwo()
	//bit.SumOfTwoIntegers()
	//easy.AddBinary()
	//easy.ReverseString()
	//easy.CountAndSay()
	//easy.ValidPalindromeII()
	//easy.IsPalindrome()
	//easy.RemoveDuplicatesFromSortedArray()
	//easy.MergeSortedArrays()
	//easy.IntersectionOfTwoLinkedLists()
	//easy.LowestCommonAncestorBST()
	//easy.SubString()
	easy.NewMaxSubArraySum()
}
