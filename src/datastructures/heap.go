package datastructures

type heap interface {
	Parent(int) int
	LeftChild(int) int
	RightChild(int) int
	GetMaximum() int
	PercolateDown(int)
	DeleteMax() int
}

type Heap struct {
	numbers  []int
	heapType int
}

func (h *Heap) Parent(i int) int {
	if i <= 0 || i >= len(h.numbers) {
		return -1
	}
	return (i - 1) / 2
}

func (h *Heap) LeftChild(i int) int {
	left := 2*i + 1
	if left >= len(h.numbers) {
		return -1
	}
	return left
}

func (h *Heap) RightChild(i int) int {
	right := 2*i + 2
	if right >= len(h.numbers) {
		return -1
	}
	return right
}

func (h *Heap) GetMaximum() int {
	if len(h.numbers) == 0 {
		return -1
	}
	return h.numbers[0]
}

func (h *Heap) PercolateDown(i int) {
	var l, r, max, temp int
	l = h.LeftChild(i)
	r = h.RightChild(i)
	if l != -1 && h.numbers[l] > h.numbers[i] {
		max = l
	} else {
		max = i
	}

	if r != -1 && h.numbers[r] > h.numbers[max] {
		max = r
	}

	if max != i {
		temp = h.numbers[i]
		h.numbers[i] = h.numbers[max]
		h.numbers[max] = temp
	}
	h.PercolateDown(max)
}

func (h *Heap) DeleteMax() int {
	if len(h.numbers) == 0 {
		return -1
	}
	//save the first value to return
	data := h.numbers[0]
	//copy the last element into first position and delete the last element.
	h.numbers[0] = h.numbers[len(h.numbers)-1]
	h.numbers = h.numbers[:len(h.numbers)-1]
	//Now percolateDown to reorder the array.
	h.PercolateDown(0)
	return data
}
