package microsoft

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrintV(t *testing.T) {
	input := 12345
	assert.NotPanics(t, func() { PrintV(input) })
	input = -12345
	assert.NotPanics(t, func() { PrintV(input) })
}

func TestAtoi8(t *testing.T) {
	input := "17"
	expected := 15
	output, err := Atoi8(input)

	assert.NoError(t, err)
	assert.Equal(t, expected, output)
}
