package microsoft

import (
	"errors"
	"fmt"
)

func PrintV(i int) {
	var stack []int
	for i != 0 {
		rem := i % 10
		stack = append(stack, rem)
		i = i / 10
	}

	for i := len(stack) - 1; i >= 0; i-- {
		fmt.Println(stack[i])
	}
}

func Atoi8(input string) (int, error) {
	if len(input) == 0 {
		return 0, nil
	}

	isNegative := false
	indexTostart := 0
	if input[indexTostart] == '-' {
		isNegative = true
		indexTostart = 1
	}

	var output int
	for i := indexTostart; i < len(input); i++ {
		c := input[i] - '0'

		if c >= 0 && c <= 7 {
			output = output*8 + int(c)
		} else {
			return 0, errors.New("invalid number")
		}
	}

	if isNegative {
		return output * -1, nil
	}
	return output, nil
}
