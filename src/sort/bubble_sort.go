package sort

func BubbleSortIncreasing(input []int) {
	n := len(input)
	//We place the biggest element to the last
	swapped := true
	for i := n - 1; i >= 0 && swapped; i-- {
		//This is for improving the performance of the bubble sort.
		swapped = false
		for j := 0; j < i; j++ {
			if input[j] > input[j+1] {
				input[j], input[j+1] = input[j+1], input[j]
				swapped = true
			}
		}
	}
}

func BubbleSortDecreasing(input []int) {
	n := len(input)
	//We place the biggest element to the last
	swapped := true
	for i := n - 1; i >= 0 && swapped; i-- {
		//This is for improving the performance of the bubble sort.
		swapped = false
		for j := 0; j < i; j++ {
			if input[j] < input[j+1] {
				input[j], input[j+1] = input[j+1], input[j]
				swapped = true
			}
		}
	}
}
