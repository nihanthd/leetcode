package sort

// This basically finds the min from the list and replace it with the current position.
func SelectionSortIncreasing(input []int) {
	n := len(input)
	var min int
	for i := 0; i < n; i++ {
		min = i
		for j := i + 1; j < n; j++ {
			if input[j] < input[min] {
				//that means there is an element in input which is smaller than the processed elements
				min = j
			}
		}
		//since we found the minimum value's position put it into ith position
		input[min], input[i] = input[i], input[min]
	}
}

func SelectionSortDecreasing(input []int) {
	n := len(input)
	var max int
	for i := 0; i < n; i++ {
		max = i
		for j := i + 1; j < n; j++ {
			if input[j] > input[max] {
				//that means there is an element in input which is smaller than the processed elements
				max = j
			}
		}
		//since we found the minimum value's position
		input[max], input[i] = input[i], input[max]
	}
}
