package sort

func InsertionSortIncreasing(input []int) {
	//We find the right place and insert the value
	n := len(input)
	for i := 1; i < n; i++ {
		key := input[i]
		j := i - 1
		//moving all the values towards right
		for j >= 0 && input[j] > key {
			input[j+1] = input[j]
			j--
		}
		input[j+1] = key
	}
}

func InsertionSortDecreasing(input []int) {
	n := len(input)
	for i := 1; i < n; i++ {
		key := input[i]
		j := i - 1
		for j >= 0 && input[j] < key {
			input[j+1] = input[j]
			j--
		}
		input[j+1] = key
	}
}
