package sort

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBubbleSortIncreasing(t *testing.T) {
	input := []int{5, 4, 6, 7, 2, 3}
	expectedOutput := []int{2, 3, 4, 5, 6, 7}

	BubbleSortIncreasing(input)

	assert.Equal(t, expectedOutput, input)
}

func TestBubbleSortDecreasing(t *testing.T) {
	input := []int{5, 4, 6, 7, 2, 2, 3}
	expectedOutput := []int{7, 6, 5, 4, 3, 2, 2}

	BubbleSortDecreasing(input)

	assert.Equal(t, expectedOutput, input)
}

func TestSelectionSortIncreasing(t *testing.T) {
	input := []int{5, 4, 6, 7, 2, 2, 3}
	expectedOutput := []int{2, 2, 3, 4, 5, 6, 7}

	SelectionSortIncreasing(input)

	assert.Equal(t, expectedOutput, input)
}

func TestSelectionSortDecreasing(t *testing.T) {
	input := []int{5, 4, 6, 7, 2, 2, 3}
	expectedOutput := []int{7, 6, 5, 4, 3, 2, 2}

	SelectionSortDecreasing(input)

	assert.Equal(t, expectedOutput, input)
}

func TestInsertionSort(t *testing.T) {
	input := []int{5, 4, 6, 7, 2, 2, 3}
	expectedOutput := []int{2, 2, 3, 4, 5, 6, 7}

	InsertionSortIncreasing(input)

	assert.Equal(t, expectedOutput, input)
}

func TestInsertionSortDecreasing(t *testing.T) {
	input := []int{5, 4, 6, 7, 2, 2, 3}
	expectedOutput := []int{7, 6, 5, 4, 3, 2, 2}

	InsertionSortDecreasing(input)

	assert.Equal(t, expectedOutput, input)
}
