package design

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSpecialStack_Push(t *testing.T) {
	s := SpecialStack{}
	s.Push(3)
	s.Push(5)
	expected := 3
	assert.Equal(t, expected, s.GetMin(), "")
}
