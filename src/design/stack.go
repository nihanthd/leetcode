package design

import (
	"errors"
	"math"
)

type stack interface {
	Push(int)
	Pop() int
	IsEmpty() bool
	IsFull() bool
	Peek() (int, error)
}

type Stack struct {
	A []int
}

func (s *Stack) Push(x int) {
	s.A = append(s.A, x)
}

func (s *Stack) IsEmpty() bool {
	if len(s.A) == 0 {
		return true
	}
	return false
}

func (s *Stack) Peek() (int, error) {
	if s.IsEmpty() {
		return 0, errors.New("the stack is empty")
	}
	return s.A[len(s.A)-1 : len(s.A)][0], nil
}

//Since arrays can be appended and stack size can be increased.
func (s *Stack) IsFull() bool {
	return false
}

func (s *Stack) Pop() int {
	if s.IsEmpty() {
		return math.MinInt32
	}
	x := s.A[len(s.A)-1]
	s.A = s.A[0 : len(s.A)-1]
	return x
}
