package design

import (
	"math"
)

type specialStack interface {
	stack
	GetMin() int
}

type SpecialStack struct {
	Stack
	Min Stack
}

func (s *SpecialStack) IsEmpty() bool {
	if len(s.Min.A) == 0 {
		return true
	}
	return false
}

func (s *SpecialStack) Pop() int {
	if s.IsEmpty() {
		return math.MinInt32
	}
	x := s.Stack.Pop()
	y := s.Min.Pop()
	if x != y {
		s.Min.Push(y)
	}
	return x
}

func (s *SpecialStack) GetMin() int {
	x := s.Min.Pop()
	s.Min.Push(x)
	return x
}

func (s *SpecialStack) Push(x int) {
	s.Stack.Push(x)
	if s.IsEmpty() {
		s.Min.A = append(s.Min.A, x)
	} else {
		y := s.Min.Pop()
		s.Min.Push(y)
		if x <= y {
			s.Min.Push(x)
		}
	}
}
