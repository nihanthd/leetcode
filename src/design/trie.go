package design

type Trie struct {
	Data      string
	Right     *Trie
	Below     *Trie
	EndOfWord bool
}

/** Initialize your data structure here. */
func Constructor() Trie {
	return Trie{
		Data:      "",
		Right:     nil,
		Below:     nil,
		EndOfWord: false,
	}
}

/** Inserts a word into the trie. */
func (this *Trie) Insert(word string) {
	//Initialization

	if this.Data == "" {
		this.Data = string(word[0])
	}
	temp := this
	for index, val := range word {
		found := false
		//Now it could be linked to this word or we have to find the Right
		for true {
			//find the root
			if temp.Data == string(val) {
				//this is the root
				found = true
				break
			}
			if temp.Right != nil {
				temp = temp.Right
			} else {
				break
			}
		}
		if found && temp.Below != nil {
			if index+1 == len(word) {
				temp.EndOfWord = true
			}
			temp = temp.Below
			continue
		} else {
			if temp.Below != nil {
				x := &Trie{
					Data:      "",
					Right:     nil,
					Below:     nil,
					EndOfWord: false,
				}
				if len(word) > index {
					temp.Right = x
					x.Insert(word[index:])
					break
				} else {
					temp.EndOfWord = true
				}
			} else {
				x := &Trie{
					Data:      "",
					Right:     nil,
					Below:     nil,
					EndOfWord: false,
				}
				if len(word) > index+1 {
					temp.Below = x
					x.Insert(word[index+1:])
					break
				} else {
					temp.EndOfWord = true
				}
			}
		}
	}
}

/** Returns if the word is in the trie. */
func (this *Trie) Search(word string) bool {
	temp := this
	for index, val := range word {
		if temp.Data == string(val) {
			//since we found it, now go below
			if temp.Below != nil && index+1 < len(word) {
				temp = temp.Below
				continue
			}
		} else if temp.Right != nil {
			found := false
			for temp != nil {
				if temp.Data == string(val) {
					found = true
					break
				}
				temp = temp.Right
			}
			if found && temp.Below != nil && index+1 < len(word) {
				temp = temp.Below
				continue
			} else {
				return false
			}
		} else {
			return false
		}
	}
	if temp.EndOfWord {
		return true
	} else {
		return false
	}
}

/** Returns if there is any word in the trie that starts with the given prefix. */
func (this *Trie) StartsWith(prefix string) bool {
	temp := this
	for _, val := range prefix {
		if temp.Data == string(val) {
			//since we found it, now go below
			if temp.Below != nil {
				temp = temp.Below
				continue
			}
		} else if temp.Right != nil {
			found := false
			for temp != nil {
				if temp.Data == string(val) {
					found = true
					break
				}
				temp = temp.Right
			}
			if found {
				temp = temp.Below
				continue
			} else {
				return false
			}
		} else {
			return false
		}
	}
	return true
}

/**
 * Your Trie object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Insert(word);
 * param_2 := obj.Search(word);
 * param_3 := obj.StartsWith(prefix);
 */
