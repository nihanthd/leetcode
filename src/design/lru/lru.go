package lru

import (
	"container/list"
)

type LRUCache struct {
	size      int
	evictList *list.List
	keyMap    map[int]*list.Element
}

func Constructor(capacity int) LRUCache {
	return LRUCache{
		size: capacity,
	}
}

func (this *LRUCache) Get(key int) int {
	return 0
}

func (this *LRUCache) Put(key int, value int) {

}
