package trie

type Trie struct {
	root *node
}

type node struct {
	wordEnd  bool
	children [26]*node
}

/** Initialize your data structure here. */
func Constructor() Trie {
	return Trie{root: &node{}}
}

/** Inserts a word into the trie. */
func (this *Trie) Insert(word string) {
	if len(word) == 0 {
		return
	}

	cur := this.root
	for i := range word {
		c := word[i] - 'a'
		n := cur.children[c]
		if n == nil {
			n = &node{}
			cur.children[c] = n
		}
		cur = n
	}
	cur.wordEnd = true
}

/** Returns if the word is in the trie. */
func (this *Trie) Search(word string) bool {
	cur := this.find(word)
	if cur == nil {
		return false
	}
	return cur.wordEnd
}

/** Returns if there is any word in the trie that starts with the given prefix. */
func (this *Trie) StartsWith(prefix string) bool {
	cur := this.find(prefix)
	return cur != nil
}

func (t *Trie) find(word string) *node {
	if len(word) == 0 {
		return nil
	}

	cur := t.root
	for i := range word {
		c := word[i] - 'a'
		cur = cur.children[c]
		if cur == nil {
			return nil
		}
	}

	return cur
}
