package trie

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConstructor(t *testing.T) {
	x := Constructor()
	assert.NotNil(t, x)
}

func TestTrie_Insert(t *testing.T) {
	root := Constructor()
	root.Insert("apple")
	root.Insert("app")
}

func TestTrie_StartsWith(t *testing.T) {
	root := Constructor()
	root.Insert("apple")
	assert.True(t, root.Search("apple"))
	assert.True(t, root.StartsWith("app"))
}
