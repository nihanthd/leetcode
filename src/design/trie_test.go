package design

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTrie(t *testing.T) {
	trie := Constructor()
	trie.Insert("implement")
	assert.Equal(t, "i", trie.Data)
	assert.Nil(t, trie.Right)
	assert.Equal(t, "m", trie.Below.Data)
	assert.Nil(t, trie.Below.Right)
	assert.Equal(t, "p", trie.Below.Below.Data)
	assert.Nil(t, trie.Below.Below.Right)
	trie.Insert("has")
	assert.NotNil(t, trie.Right)
	trie.Insert("important")
	trie.Insert("implementation")
	assert.True(t, trie.StartsWith("h"))
	assert.False(t, trie.StartsWith("nih"))

	trie = Constructor()
	trie.Insert("apple")
	assert.True(t, trie.Search("apple"))
	assert.False(t, trie.Search("app"))
	assert.True(t, trie.StartsWith("app"))
	trie.Insert("app")
	assert.True(t, trie.Search("app"))
	assert.False(t, trie.Search("apples"))
}

func TestConstructor(t *testing.T) {
	trie := Constructor()
	assert.Equal(t, "", trie.Data)
	assert.Nil(t, trie.Right)
	assert.Nil(t, trie.Below)
}
