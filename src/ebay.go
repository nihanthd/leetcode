package main

import "fmt"

func main() {
	str1 := "ABC"
	str2 := "AFC"
	output := count(str1, str2)

	fmt.Printf("%d\n", output)
	fmt.Printf("%d\n", count("EACBD", "EABCD"))
	fmt.Printf("%d\n", count("EACBD", "EA"))
	fmt.Printf("%d\n", count("", "EA"))
	fmt.Printf("%d\n", count("EACBD", ""))
	fmt.Printf("%d\n", count("", "EA"))
	fmt.Printf("%d\n", count("", ""))
	fmt.Printf("%d\n", count("", "EA"))
	fmt.Printf("%d\n", count("ABCD", "EFGH"))
	fmt.Printf("%d\n", count("ABCDEF", "CCCDEF"))
	fmt.Printf("%d\n", count("ABCD", "CCCDEF"))
	fmt.Printf("%d\n", count("horse", "ros"))
	fmt.Printf("%d\n", count("intention", "execution"))
}

func count(word1, word2 string) int {
	//lengths are different
	if len(word1) == 0 {
		return len(word2)
	}

	if len(word2) == 0 {
		return len(word1)
	}

	matrix := make([][]int, len(word1))
	for i := 0; i < len(matrix); i++ {
		matrix[i] = make([]int, len(word2))
	}
	for i := 0; i < len(word1); i++ {
		for j := 0; j < len(word2); j++ {
			if word1[i] != word2[j] {
				matrix[i][j] = 1
			}
		}
	}
	result := matrix[len(word1)-1][len(word2)-1]
	i := len(word1) - 1
	j := len(word2) - 1
	for i > 0 && j > 0 {
		if matrix[i-1][j] > matrix[i][j-1] {
			j--
		} else if matrix[i-1][j] < matrix[i][j-1] {
			i--
		} else {
			i--
			j--
		}
		result = result + matrix[i][j]
	}

	return result
}
