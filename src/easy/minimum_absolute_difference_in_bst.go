package easy

import (
	"fmt"
	"math"
)

func getMinimumDifference(root *TreeNode) int {
	difference := math.MaxInt32
	nums := traversal(root)
	for i, j := 0, 1; j < len(nums); {
		newDiff := nums[j] - nums[i]
		if newDiff < difference {
			difference = newDiff
		}
		i++
		j++
	}
	return difference
}

func traversal(root *TreeNode) []int {
	var nums []int
	if root == nil {
		return nums
	}
	leftNums := traversal(root.Left)
	nums = append(nums, leftNums...)
	nums = append(nums, root.Val)
	rightNums := traversal(root.Right)
	nums = append(nums, rightNums...)
	return nums
}

func GetMinimumDifference() {
	//root := &TreeNode{
	//	Val:  1,
	//	Left: nil,
	//	Right: &TreeNode{
	//		Val: 3,
	//		Left: &TreeNode{
	//			Val:   2,
	//			Left:  nil,
	//			Right: nil,
	//		},
	//		Right: nil,
	//	},
	//}

	root := &TreeNode{
		Val: 50,
		Left: &TreeNode{
			Val: 20,
			Left: &TreeNode{
				Val:   5,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   40,
				Left:  nil,
				Right: nil,
			},
		},
		Right: &TreeNode{
			Val: 80,
			Left: &TreeNode{
				Val:   65,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   115,
				Left:  nil,
				Right: nil,
			},
		},
	}
	difference := getMinimumDifference(root)
	fmt.Printf("Minimum difference in a BST = %d\n", difference)
}
