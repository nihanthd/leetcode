package easy

import "math"

/*
Given a binary tree find the maximum difference between node and its ancestor
*/

func findMaximumDifference(root *TreeNode, maxNode *TreeNode) int {
	if root == nil {
		return math.MinInt32
	}
	leftDiff, rightDiff := math.MinInt32, math.MinInt32
	var leftMaxNode, rightMaxNode *TreeNode
	if root.Left != nil {
		leftDiff = root.Val - root.Left.Val

		if root.Left.Val > root.Right.Val {
			leftMaxNode = root.Left
		} else {
			leftMaxNode = root
		}
	}

	if root.Right != nil {
		rightDiff = root.Val - root.Right.Val

		if root.Right.Val > root.Val {
			rightMaxNode = root.Right
		} else {
			rightMaxNode = root
		}
	}

	leftMax := findMaximumDifference(root.Left, leftMaxNode)
	rightMax := findMaximumDifference(root.Right, rightMaxNode)

	return max(max(leftMax, rightMax), max(leftDiff, rightDiff))
}
