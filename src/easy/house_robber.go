package easy

/*
https://leetcode.com/problems/house-robber/
*/

func Rob(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	sums := make([]int, len(nums))
	if len(sums) >= 1 {
		sums[0] = nums[0]
	}
	if len(sums) >= 2 {
		sums[1] = max(nums[0], nums[1])

		for i := 2; i < len(nums); i++ {
			sums[i] = max(sums[i-2]+nums[i], sums[i-1])
		}
	}

	return sums[len(sums)-1]
}
