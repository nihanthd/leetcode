package easy

func FlipAndInvertImage(A [][]int) [][]int {
	var temp int
	for _, row := range A {
		//reverse
		for i, j := 0, len(row)-1; i <= j; i, j = i+1, j-1 {
			temp = row[i]
			row[i] = row[j]
			row[j] = temp
		}
	}

	for _, row := range A {
		//reverse
		for index, val := range row {
			if val == 0 {
				row[index] = 1
			} else {
				row[index] = 0
			}
		}
	}
	return A
}
