package easy

/*
https://leetcode.com/problems/ransom-note/submissions/
*/
func canConstruct(ransomNote string, magazine string) bool {
	result := true

	//in the first pass we process magazine and store the count of each alphabet in map
	magazineMap := make(map[rune]int)
	for _, character := range magazine {
		if _, ok := magazineMap[character]; ok {
			magazineMap[character] = magazineMap[character] + 1
		} else {
			magazineMap[character] = 1
		}
	}

	//now we try to find if the ransom note can be constructed using the magazineMap
	//if at any point we cannot find a character from ransom note in the magazineMap then we can return false
	//Another condition would be if the character is available and count is zero
	for _, character := range ransomNote {
		if count, ok := magazineMap[character]; ok {
			if count <= 0 {
				//character got consumed
				return false
			} else {
				magazineMap[character] = count - 1
			}
		} else {
			//character is not found
			return false
		}
	}

	return result
}

func canConstructWithArray(ransomNote string, magazine string) bool {
	result := true
	//in the first pass we process magazine and store the count of each alphabet in map
	magazineMap := make([]int, 26)
	for _, character := range magazine {
		magazineMap[character-'a'] = magazineMap[character-'a'] + 1
	}

	//now we try to find if the ransom note can be constructed using the magazineMap
	//if at any point we cannot find a character from ransom note in the magazineMap then we can return false
	//Another condition would be if the character is available and count is zero
	for _, character := range ransomNote {
		index := character - 'a'
		if value := magazineMap[index]; value > 0 {
			magazineMap[index] = value - 1
		} else {
			return false
		}
	}

	return result
}
