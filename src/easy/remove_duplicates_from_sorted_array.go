package easy

import (
	"fmt"
)

func removeDuplicates(nums []int) int {
	count := len(nums)
	if count == 0 {
		return 0
	}
	//We take the first number and count it as 1
	uniqueNumCount := 1
	previousNum := nums[0]

	//We need last index to identify until which number we can process the nums array
	lastIndex := count - 1
	//Now we loop over the numbers to find the repeated nums and move them to the last of the array.
	for i := 1; i <= lastIndex; {
		currentNum := nums[i]
		if currentNum == previousNum {
			shiftValues(&nums, i)
			lastIndex--
		} else {
			previousNum = nums[i]
			i++
			uniqueNumCount++
		}
	}
	return uniqueNumCount
}

func shiftValues(nums *[]int, i int) {
	for j := i; j < len(*nums)-1; j++ {
		temp := (*nums)[j]
		(*nums)[j] = (*nums)[j+1]
		(*nums)[j+1] = temp
	}
}

func RemoveDuplicatesFromSortedArray() {
	nums := []int{1, 1, 2}
	output := removeDuplicates(nums)
	for i := 0; i < output; i++ {
		fmt.Println(nums[i])
	}
}
