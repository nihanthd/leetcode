package easy

import "fmt"

func InorderTraversal(root *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}
	InorderTraversal(root.Left)
	fmt.Println(root.Val)
	InorderTraversal(root.Right)
	return root
}
