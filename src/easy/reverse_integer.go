package easy

import (
	"fmt"
	"math"
)

func NewReverseNumber() {
	intMax := math.Pow(2, 31) - 1
	intMin := math.Pow(2, 31) * -1
	x := -2147483648
	number := 0
	isNumberNegative := x < 0
	if isNumberNegative {
		x = x * -1
	}
	for x != 0 {
		rem := x % 10
		number = number*10 + rem
		x = x / 10
	}

	if isNumberNegative {
		number = number * -1
	}
	if number > int(intMax) || number < int(intMin) {
		number = 0
	}
	fmt.Println(number)
}
