package easy

import (
	"fmt"
)

func findPairs(nums []int, k int) int {
	var pairs int
	if nums == nil || len(nums) < 2 || k < 0 {
		return pairs
	}
	diff := make(map[int]int)
	for _, n := range nums {
		if k > 0 && diff[n] == 0 {
			if diff[n-k] > 0 {
				pairs++
			}
			if diff[n+k] > 0 {
				pairs++
			}
		}
		diff[n]++
	}

	if k == 0 {
		for _, val := range diff {
			if val > 1 {
				pairs++
			}
		}
	}

	return pairs
}

func FindPairs() {
	pairs := findPairs([]int{3, 1, 4, 1, 5}, 2)
	fmt.Printf("Total Number of unique pairs found = %d\n", pairs)
}
