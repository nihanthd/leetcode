package easy

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMergeTrees(t *testing.T) {
	t1 := &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val:  3,
			Left: &TreeNode{Val: 5},
		},
		Right: &TreeNode{
			Val: 2,
		},
	}

	t2 := &TreeNode{
		Val: 2,
		Left: &TreeNode{
			Val: 1,
			Right: &TreeNode{
				Val: 4,
			},
		},
		Right: &TreeNode{Val: 3, Right: &TreeNode{
			Val: 7,
		}}}

	expected := &TreeNode{
		Val:   3,
		Left:  &TreeNode{Val: 4, Left: &TreeNode{Val: 5}, Right: &TreeNode{Val: 4}},
		Right: &TreeNode{Val: 5, Right: &TreeNode{Val: 7}},
	}
	actual := MergeTrees(t1, t2)

	assert.Equal(t, expected, actual)
}
