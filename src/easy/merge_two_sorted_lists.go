package easy

import (
	"fmt"
)

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	var head, node *ListNode
	for l1 != nil || l2 != nil {

		var val *ListNode

		if (l1 != nil && l2 != nil && l1.Val <= l2.Val) || l2 == nil {
			val = l1
			if l1 != nil {
				l1 = l1.Next
			}
		} else if l2 != nil || l1 == nil {
			val = l2
			if l2 != nil {
				l2 = l2.Next
			}
		}
		if node == nil {
			node = val
			head = node
		} else {
			node.Next = val
			node = node.Next
		}
	}
	return head
}

func NewMergeTwoSortedLists() {
	//l1 := &ListNode{
	//	Val: 1,
	//	Next: &ListNode{
	//		Val: 2,
	//		Next: &ListNode{
	//			Val:  4,
	//			Next: nil,
	//		},
	//	},
	//}
	//
	//l2 := &ListNode{
	//	Val: 1,
	//	Next: &ListNode{
	//		Val: 3,
	//		Next: &ListNode{
	//			Val:  4,
	//			Next: nil,
	//		},
	//	},
	//}
	l1 := &ListNode{
		Val:  1,
		Next: nil,
	}

	var l2 *ListNode
	l3 := mergeTwoLists(l1, l2)
	for l3 != nil {
		fmt.Println(l3.Val)
		l3 = l3.Next
	}
}
