package easy

import (
	"fmt"
	"strconv"
	"strings"
)

/*
https://leetcode.com/problems/ip-to-cidr/description/
*/
func IpToCIDR(ip string, n int) []string {
	start := ipToint64(ip)
	var result []string
	for n > 0 {
		mask := max(33-bitLength(int(rightMostSetBit(start))),
			33-bitLength(n))

		result = append(result, int64ToIp(start)+"/"+strconv.FormatInt(int64(mask), 10))
		shift := 32 - mask
		diff := 1 << uint(shift)
		start = start + int64(diff)
		n = n - diff
	}
	return result
}

func rightMostSetBit(n int64) int64 {
	return n & -n
}

func ipToint64(ip string) int64 {
	var ans int64
	ips := strings.Split(ip, ".")
	for _, x := range ips {
		intx, _ := strconv.Atoi(x)
		ans = 256*ans + int64(intx)
	}
	return ans
}

func int64ToIp(x int64) string {
	return fmt.Sprintf("%d.%d.%d.%d",
		x>>24, (x>>16)%256, (x>>8)%256, x%256)
}

func bitLength(x int) int {
	ans := 0
	if x == 0 {
		return 1
	}

	for x > 0 {
		x >>= 1
		ans++
	}
	return ans
}
