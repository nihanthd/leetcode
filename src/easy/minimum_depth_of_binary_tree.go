package easy

import (
	"fmt"
	"math"
)

func minDepth(root *TreeNode) int {
	var depth int
	if root == nil {
		return 0
	}
	depth++
	leftDepth := minDepth(root.Left)
	rightDepth := minDepth(root.Right)
	if leftDepth == 0 {
		return depth + rightDepth
	}
	if rightDepth == 0 {
		return depth + leftDepth
	}
	depth = depth + int(math.Min(float64(leftDepth), float64(rightDepth)))
	return depth
}

func MinimumDepthOfABinaryTree() {
	root := &TreeNode{
		Val: 3,
		Left: &TreeNode{
			Val:   9,
			Left:  nil,
			Right: nil,
		},
		Right: &TreeNode{
			Val: 20,
			Left: &TreeNode{
				Val:   15,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   7,
				Left:  nil,
				Right: nil,
			},
		},
	}
	//root := &TreeNode{
	//	Val: 3,
	//	Left: &TreeNode{
	//		Val: 3,
	//	},
	//	Right: nil,
	//}
	depth := minDepth(root)
	fmt.Printf("The minimum depth of the tree is %d\n", depth)
}
