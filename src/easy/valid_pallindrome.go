package easy

import (
	"fmt"
)

/*
https://leetcode.com/problems/valid-palindrome/description/
*/

func isPalindromediff(s string) bool {
	//pointer at beginning and end
	//for i, j := 0, len(s)-1; i <= j; {
	//	//if the character to the right is not a alphanumeric...increase until the next character
	//	for i < len(s) && (s[i] < '0' || (s[i] > '9' && s[i] < 'A') || (s[i] > 'Z' && s[i] < 'a') || s[i] > 'z') {
	//		i++
	//	}
	//	//if the character to the left is not a alphanumeric...decrease until the next character
	//	for j >= 0 && (s[j] < '0' || (s[j] > '9' && s[j] < 'A') || (s[j] > 'Z' && s[j] < 'a') || s[j] > 'z') {
	//		j--
	//	}
	//	// we will increment i and decrement j based on the character
	//	if s[i] != s[j] && s[i]+32 != s[j] && s[i]-32 != s[j] {
	//		return false
	//	}
	//
	//	//increase to get to the actual alphanumeric character
	//	i++
	//
	//	//decrease to get to the actual alphanumeric character
	//	j--
	//}

	//construct a new string with just alphanumeric characters
	var input []byte
	for i := 0; i < len(s); i++ {
		if (s[i] >= '0' && s[i] <= '9') || (s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z') {
			input = append(input, s[i])
		}
	}

	for i, j := 0, len(input)-1; i < j; i, j = i+1, j-1 {
		if input[i] != input[j] {
			if input[i] > '9' && input[j] > '9' && (input[i]+32 == input[j] || input[i]-32 == input[j]) {
				continue
			}
			return false
		}
	}
	return true
}

func IsPalindrome() {
	input := "race a car"
	output := isPalindromediff(input)
	fmt.Printf("is Input - '%s' a pallindrome? %t\n", input, output)
}
