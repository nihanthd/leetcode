package easy

func longestUnivaluePath(root *TreeNode) int {
	if root == nil {
		return 0
	}
	longestUnivaluePath(root.Left)
	return -1
}
