package easy

import (
	"math"
)

func PowerfulIntegers(x int, y int, bound int) []int {
	var result []int
	resultMap := make(map[int]bool)
	xPow := make(map[int]bool)
	yPow := make(map[int]bool)
	xFloat := float64(x)
	yFloat := float64(y)
	if x != 1 {
		for i := 0.0; true; i++ {
			val := int(math.Pow(xFloat, i))
			if val >= bound {
				break
			}
			xPow[val] = true
		}
	} else {
		xPow[1] = true
	}
	if y != 1 {
		for i := 0.0; true; i++ {
			val := int(math.Pow(yFloat, i))
			if val >= bound {
				break
			}
			yPow[val] = true
		}
	} else {
		yPow[1] = true
	}

	for xkey, _ := range xPow {
		for yKey, _ := range yPow {
			val := xkey + yKey
			if val <= bound {
				if _, ok := resultMap[val]; !ok {
					result = append(result, val)
					resultMap[val] = true
				}
			}
		}
	}
	return result
}
