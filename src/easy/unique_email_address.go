package easy

import (
	"strings"
)

/*
https://leetcode.com/problems/unique-email-addresses/description/
*/
func NumUniqueEmails(emails []string) int {
	emailsMap := make(map[string]bool)
	for _, email := range emails {
		processedEmail := processEmail(email)
		emailsMap[processedEmail] = true
	}
	return len(emailsMap)
}

func processEmail(email string) string {
	emailParts := strings.Split(email, "@")
	localName, domainName := emailParts[0], emailParts[1]
	localName = strings.Split(localName, "+")[0]
	localName = strings.Replace(localName, ".", "", -1)
	return localName + "@" + domainName
}
