package easy

import "fmt"

func isBalanced(root *TreeNode) bool {
	if root == nil {
		return true
	}
	return isBalanced(root.Left) && isBalanced(root.Right) && abs(depthOfTree(root.Left), depthOfTree(root.Right)) < 2
}

func depthOfTree(root *TreeNode) int {
	if root == nil {
		return 0
	}
	return max(depthOfTree(root.Left), depthOfTree(root.Right)) + 1
}

func BalancedBinaryTree() {
	//root := &TreeNode{
	//	Val: 3,
	//	Left: &TreeNode{
	//		Val:   9,
	//		Left:  nil,
	//		Right: nil,
	//	},
	//	Right: &TreeNode{
	//		Val: 20,
	//		Left: &TreeNode{
	//			Val:   15,
	//			Left:  nil,
	//			Right: nil,
	//		},
	//		Right: &TreeNode{
	//			Val:   7,
	//			Left:  nil,
	//			Right: nil,
	//		},
	//	},
	//}
	root := &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val: 2,
			Left: &TreeNode{
				Val: 3,
				Left: &TreeNode{
					Val:   4,
					Left:  nil,
					Right: nil,
				},
				Right: &TreeNode{
					Val:   4,
					Left:  nil,
					Right: nil,
				},
			},
			Right: &TreeNode{
				Val:   3,
				Left:  nil,
				Right: nil,
			},
		},
		Right: &TreeNode{
			Val:   2,
			Left:  nil,
			Right: nil,
		},
	}
	isBalancedBinaryTree := isBalanced(root)
	fmt.Printf("The given tree is balanced = %t\n", isBalancedBinaryTree)
}
