package easy

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func (l *ListNode) Print(){
	for l != nil {
		fmt.Print(l.Val)
		l = l.Next
		if l!= nil {
			fmt.Print(" -> ")
		}
	}
	fmt.Println()
}
