package easy

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrintCharacters(t *testing.T) {
	assert.NotPanics(t, func() { PrintCharacters() }, "")
}
