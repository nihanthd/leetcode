package bit

func isPowerOfTwo(n int) bool {
	if n <= 0 {
		return false
	}
	if (n & -n) != n {
		return false
	} else {
		return true
	}
}
