package bit

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPowerOfTwo(t *testing.T) {
	num := 9
	result := isPowerOfTwo(num)
	assert.False(t, result)

	num = 8
	result = isPowerOfTwo(num)
	assert.True(t, result)

	num = -1
	result = isPowerOfTwo(num)
	assert.False(t, result)

}

func TestSumOfTwoIntegers(t *testing.T) {
	a, b := 3, -2
	sum := getSum(a, b)
	expectedSum := 1
	assert.Equal(t, expectedSum, sum)

	a, b = 0, -2
	sum = getSum(a, b)
	expectedSum = -2
	assert.Equal(t, expectedSum, sum)

	a, b = 3, 0
	sum = getSum(a, b)
	expectedSum = 3
	assert.Equal(t, expectedSum, sum)
}
