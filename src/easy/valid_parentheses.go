package easy

func isValid(s string) bool {
	var stack []rune
	runeMap := map[rune]rune{']': '[', '}': '{', ')': '('}
	for _, rune := range s {
		if rune == '{' || rune == '[' || rune == '(' {
			//since it is opening braces, then insert it into top of stack.
			stack = append(stack, rune)
		} else if len(stack) > 0 {
			popped := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			if popped != runeMap[rune] {
				return false
			}
		} else {
			return false
		}
	}
	if len(stack) > 0 {
		return false
	} else {
		return true
	}
}
