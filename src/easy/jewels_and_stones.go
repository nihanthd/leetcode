package easy

func NumJewelsInStones(J string, S string) int {
	count := 0
	stoneMap := make(map[rune]bool)
	for _, char := range J {
		stoneMap[char] = true
	}
	for _, char := range S {
		if _, ok := stoneMap[char]; ok {
			count++
		}
	}
	return count
}
