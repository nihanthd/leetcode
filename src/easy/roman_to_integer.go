package easy

import "fmt"

func romanToInt(s string) int {
	values := map[byte]int{
		'I': 1,
		'V': 5,
		'X': 10,
		'L': 50,
		'C': 100,
		'D': 500,
		'M': 1000,
	}

	var out, prev int
	for i := len(s) - 1; i >= 0; i-- {
		cur := values[s[i]]
		if cur < prev {
			out = out - cur
		} else {
			out = out + cur
		}
		prev = cur
	}
	return out
}

func RomanToInt() {
	x := romanToInt("MCMXCIV")
	fmt.Println(x)
}
