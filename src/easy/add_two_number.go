package easy

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	sum := 0
	var l3, node *ListNode
	for l1 != nil || l2 != nil {
		if l1 != nil {
			sum += l1.Val
			l1 = l1.Next
		}

		if l2 != nil {
			sum += l2.Val
			l2 = l2.Next
		}

		val := sum % 10
		sum = (sum - val) / 10
		if node == nil {
			node = &ListNode{}
			l3 = node
		} else {
			node.Next = &ListNode{}
			node = node.Next
		}
		node.Val = val
	}

	if sum > 0 {
		node.Next = &ListNode{Val: sum}
	}
	return l3
}
