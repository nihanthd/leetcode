package easy

/*
https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
*/

func maxProfit(prices []int) int {
	//Idea is to keep track of min price and if we encounter a minimum price then save it.
	//From there try to find the max difference between current value and min price,
	// which will give us a profit and track it with max profit.
	maximumProfit := 0
	minPrice := 1<<31 - 1 //Min Int 32
	for i := 0; i < len(prices); i++ {
		if prices[i] < minPrice {
			minPrice = prices[i]
		} else if (prices[i] - minPrice) > maximumProfit {
			//if we come across a min price then we have to skip profit calculation as that would be a reset.
			maximumProfit = prices[i] - minPrice
		}
	}
	return maximumProfit
}
