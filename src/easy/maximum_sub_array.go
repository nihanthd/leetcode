package easy

import (
	"fmt"
	"math"
)

func findMaxSubArray(nums []int) int {
	maxSum := math.MinInt64
	maxCur := maxSum
	for i := 0; i < len(nums); i++ {
		if maxCur < 0 {
			maxCur = nums[i]
		} else {
			maxCur += nums[i]
		}
		if maxCur > maxSum {
			maxSum = maxCur
		}
	}
	return maxSum
}

func NewMaxSubArraySum() {
	maxSum := findMaxSubArray([]int{-2, -3, -1, -5})
	fmt.Println("Maximum Sum for a sub Array - ", maxSum)
}
