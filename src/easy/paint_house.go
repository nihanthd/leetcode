package easy

/*
https://leetcode.com/problems/paint-house/
*/

func MinCost(costs [][]int) int {
	if len(costs) == 0 {
		return 0
	}
	house := costs[0]
	minCost := 1<<32 - 1
	//here we have to pick a minimum cost and its color
	for j := 0; j < len(house); j++ {
		curMin, prevColor := house[j], j
		cost := curMin
		for i := 1; i < len(costs); i++ {
			tempHouse := costs[i]
			if prevColor == 0 {
				curMin, prevColor = minColorCost(tempHouse[1], tempHouse[2], 1, 2)
			} else if prevColor == 1 {
				curMin, prevColor = minColorCost(tempHouse[0], tempHouse[2], 0, 2)
			} else {
				curMin, prevColor = minColorCost(tempHouse[0], tempHouse[1], 0, 1)
			}
			cost = cost + curMin
		}
		if cost < minCost {
			minCost = cost
		}
	}
	return minCost
}

func minColorCost(cost1, cost2, col1, col2 int) (int, int) {
	if cost1 < cost2 {
		return cost1, col1
	}
	return cost2, col2
}
