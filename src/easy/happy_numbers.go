package easy

/*
https://leetcode.com/problems/happy-number/
*/

func IsHappy(n int) bool {
	numbersVisited := make(map[int]bool)
	for true {
		if n == 1 {
			return true
		}
		if _, ok := numbersVisited[n]; ok {
			break
		} else {
			numbersVisited[n] = true
			n = calculateHappy(n)
		}
	}

	return false
}

func calculateHappy(n int) int {
	result := 0
	for n != 0 {
		number := n % 10
		n = n / 10
		result += number * number
	}
	return result
}
