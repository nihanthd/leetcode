package easy

func IsAlienSorted(words []string, order string) bool {
	orderMap := make(map[rune]int)
	for index, char := range order {
		orderMap[char] = index
	}
	for i := 0; i < len(words)-1; i++ {
		firstWord := words[i]
		secondWord := words[i+1]
		maxIterate := min(len(firstWord), len(secondWord))
		isPrefix := true
		for j := 0; j < maxIterate; j++ {
			if firstWord[j] != secondWord[j] {
				isPrefix = false
				firstChar := rune(firstWord[j])
				secondChar := rune(secondWord[j])
				firstCharOrder, _ := orderMap[firstChar]
				secondCharOrder, _ := orderMap[secondChar]
				if firstCharOrder > secondCharOrder {
					return false
				}
				break
			}
		}
		//even after max iterate we cannot determine the order, then determine by length
		if isPrefix && len(firstWord) > len(secondWord) {
			return false
		}
	}
	return true
}
