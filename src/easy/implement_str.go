package easy

/*
https://leetcode.com/problems/implement-strstr/description/
*/

func strStr(haystack string, needle string) int {
	if len(needle) == 0 {
		return 0
	}

	if len(haystack) < len(needle) {
		return -1
	}

	index := -1
	for i := 0; i <= len(haystack)-len(needle); i++ {
		found := true
		for j, k := i, 0; k < len(needle); j, k = j+1, k+1 {
			if haystack[j] != needle[k] {
				found = false
				break
			}
		}
		if found {
			index = i
			break
		}
	}
	return index
}
