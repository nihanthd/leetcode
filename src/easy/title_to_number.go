package easy

func titleToNumber(s string) int {
	result := 0
	for i := 0; i < len(s); i++ {
		result = result*26 + (int(s[i]) - 64)
	}
	return result
}
