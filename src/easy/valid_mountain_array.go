package easy

/*
https://leetcode.com/problems/valid-mountain-array/
*/

func ValidMountainArray(A []int) bool {
	//We need an array of at least length 3
	if len(A) <= 2 {
		return false
	}
	//We have to find the index of the element which is highest
	index := 0
	for i := 1; i < len(A); i++ {
		if A[i] > A[i-1] {
			index++
		} else {
			break
		}
	}
	//Since there is at least no single
	if index >= len(A)-1 || index == 0 {
		return false
	}
	//we have to check if all of the elements after the index are not decreasing
	for i := index; i+1 <= len(A)-1; i++ {
		if A[i+1] >= A[i] {
			return false
		}
	}
	//increasing := true
	//for i := 0; i <= len(A)-1; i++ {
	//	res := A[i+1] - A[i]
	//	if increasing {
	//		if  > 0 {
	//			continue
	//		} else if A[i+1] - A[i] == 0 {
	//			return false
	//		}
	//	} else {
	//
	//	}
	//}
	return true
}
