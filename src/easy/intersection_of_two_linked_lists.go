package easy

import (
	"fmt"
)

func getIntersectionNode(head1, head2 *ListNode) *ListNode {
	if head1 == nil || head2 == nil {
		return nil
	}
	temp1, temp2 := head1, head2
	for true {
		//fmt.Println(reflect.ValueOf(temp1).Elem())
		//fmt.Println(reflect.ValueOf(temp2).Elem())
		if temp1 == temp2 {
			break
		}

		//fmt.Println("==============")
		if temp1.Next == nil {
			temp1 = head2
		} else {
			temp1 = temp1.Next
		}
		if temp2.Next == nil {
			temp2 = head1
		} else {
			temp2 = temp2.Next
		}
	}
	return temp1
}

func IntersectionOfTwoLinkedLists() {
	intersectionList := &ListNode{
		Val: 5,
		Next: &ListNode{
			Val: 6,
			Next: &ListNode{
				Val:  7,
				Next: nil,
			},
		},
	}

	head1 := &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 2,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val:  4,
					Next: intersectionList,
				},
			},
		},
	}
	head2 := &ListNode{
		Val: 2,
		Next: &ListNode{
			Val: 3,
			Next: &ListNode{
				Val:  4,
				Next: intersectionList,
			},
		},
	}
	intersectionPoint := getIntersectionNode(head1, head2)
	fmt.Println(intersectionPoint)
}
