package easy

import (
	"sort"
)

func KClosest(points [][]int, K int) [][]int {
	var result [][]int
	sort.Slice(points, func(i, j int) bool {
		a := points[i]
		b := points[j]
		distA := (a[0]-0)*(a[0]-0) + (a[1]-0)*(a[1]-0)
		distB := (b[0]-0)*(b[0]-0) + (b[1]-0)*(b[1]-0)
		return distA < distB
	})
	index := 0
	for K > 0 {
		result = append(result, points[index])
		index++
		K--
	}
	return result
}
