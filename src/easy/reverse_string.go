package easy

import (
	"bytes"
	"fmt"
)

func reverseString(s string) string {
	var output bytes.Buffer
	for i := len(s) - 1; i >= 0; i-- {
		output.WriteByte(s[i])
	}
	return output.String()
}

func ReverseString() {
	input := "hello"
	output := reverseString(input)
	fmt.Println("The reversed string of", input, "is", output)
}
