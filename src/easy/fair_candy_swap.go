package easy

func fairCandySwap(A []int, B []int) []int {
	//Lets say A has to give x to B and B has to give y to A
	//Now after the exchange the sum of both the arrays should be equal
	//so the expression becomes sumA-x+y = sumB-y+x
	//for each value of x in A find if there is a value y in B

	//lets put all the values of B in a map
	mapB := make(map[int]bool)
	var sumA, sumB int
	for _, candy := range B {
		mapB[candy] = true
		sumB = sumB + candy
	}

	for _, candy := range A {
		sumA = sumA + candy
	}

	delta := (sumB - sumA) / 2
	for _, x := range A {
		if _, ok := mapB[x+delta]; ok {
			return []int{x, x + delta}
		}
	}
	return nil
}
