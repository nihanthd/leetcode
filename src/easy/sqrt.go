package easy

import "fmt"

func mySqrt(x int) int {
	if x == 0 || x == 1 {
		return x
	}

	left := 1
	right := x
	var mid int

	for left < right {
		mid = (left + right) / 2
		if mid*mid <= x && x < (mid+1)*(mid+1) {
			return mid
		} else if mid*mid < x {
			left = mid
		} else {
			right = mid
		}
	}

	return left
}

func NewMySqrt() {
	x := mySqrt(10)
	fmt.Println(x)
}
