package easy

import (
	"fmt"
	"strconv"
)

func FindMedianOfTwoEqualLengthArrays() {
	a := []int{1, 12, 15, 26, 38}
	b := []int{2, 13, 17, 30, 45}

	median := findMedianOfTwoEqualLengthArraysByTraversal(a, b)
	fmt.Println(median)
}

func findMedianOfTwoEqualLengthArraysByTraversal(a, b []int) float64 {
	n := len(a)
	var m1, m2 float64
	for i, j, count := 0, 0, 0; count <= n; count++ {
		if i == n {
			m1 = m2
			m2 = float64(b[0])
			break
		} else if j == n {
			m1 = m2
			m2 = float64(a[0])
			break
		}

		if a[i] < b[j] {
			m1 = m2
			m2 = float64(a[i])
			i++
		} else {
			m1 = m2
			m2 = float64(b[j])
			j++
		}

	}
	fmt.Println(strconv.FormatInt(int64(m1), 10) + " - " + strconv.FormatInt(int64(m2), 10))
	return (m1 + m2) / 2
}
