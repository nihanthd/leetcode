package easy

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRansomNote(t *testing.T) {
	ransomNote, magazine := "aa", "aab"
	output := canConstruct(ransomNote, magazine)
	assert.True(t, output, "")
}

func TestRansomNoteWithArray(t *testing.T) {
	ransomNote, magazine := "aa", "aab"
	output := canConstructWithArray(ransomNote, magazine)
	assert.True(t, output, "")
}

func TestAddBinary(t *testing.T) {
	input1, input2 := "110", "1"
	output := addBinary(input1, input2)
	expectedOutput := "111"
	assert.Equal(t, expectedOutput, output, "Add Binary is not working")

	input1, input2 = "1", "1"
	output = addBinary(input1, input2)
	expectedOutput = "10"
	assert.Equal(t, expectedOutput, output, "Add Binary is not working")

	input1, input2 = "1", "10"
	output = addBinary(input1, input2)
	expectedOutput = "11"
	assert.Equal(t, expectedOutput, output, "Add Binary is not working")

	input1, input2 = "10", "10"
	output = addBinary(input1, input2)
	expectedOutput = "100"
	assert.Equal(t, expectedOutput, output, "Add Binary is not working")

	input1, input2 = "11", "11"
	output = addBinary(input1, input2)
	expectedOutput = "110"
	assert.Equal(t, expectedOutput, output, "Add Binary is not working")

	input1, input2 = "101", "11"
	output = addBinary(input1, input2)
	expectedOutput = "1000"
	assert.Equal(t, expectedOutput, output, "Add Binary is not working")

	input1, input2 = "101", "101"
	output = addBinary(input1, input2)
	expectedOutput = "1010"
	assert.Equal(t, expectedOutput, output, "Add Binary is not working")
}

func TestBestTimeToBuyAndSellStock(t *testing.T) {
	prices := []int{7, 1, 5, 3, 6, 4}
	profit := maxProfit(prices)
	expectedProfit := 5
	assert.Equal(t, expectedProfit, profit)

	prices = []int{7, 6, 4, 3, 1}
	profit = maxProfit(prices)
	expectedProfit = 0
	assert.Equal(t, expectedProfit, profit)

	prices = []int{2, 1, 2, 0, 1}
	expectedProfit = 1
	profit = maxProfit(prices)
	assert.Equal(t, expectedProfit, profit)
}

func TestImplementstrstr(t *testing.T) {
	haystack := "hello"
	needle := "ll"
	index := strStr(haystack, needle)
	expected := 2
	assert.Equal(t, expected, index, "hello")

	haystack = "aaa"
	needle = "aaa"
	index = strStr(haystack, needle)
	expected = 0
	assert.Equal(t, expected, index, "aaa")

	haystack = "aaaaa"
	needle = "bba"
	index = strStr(haystack, needle)
	expected = -1
	assert.Equal(t, expected, index, "aaaa")
}

func TestValidParentheses(t *testing.T) {
	input := "()"
	output := isValid(input)
	assert.True(t, output)

	input = "()[]{}"
	output = isValid(input)
	assert.True(t, output)

	input = "(]"
	output = isValid(input)
	assert.False(t, output)

	input = "([)]"
	output = isValid(input)
	assert.False(t, output)

	input = "{[]}"
	output = isValid(input)
	assert.True(t, output)

	input = ""
	output = isValid(input)
	assert.True(t, output)
}

func TestFindMaximumDifference(t *testing.T) {
	root := &TreeNode{
		Val: 2,
		Left: &TreeNode{
			Val:   4,
			Left:  nil,
			Right: nil,
		},
		Right: &TreeNode{
			Val:   3,
			Left:  nil,
			Right: nil,
		},
	}

	fmt.Println(findMaximumDifference(root, root))
}

func TestIpToCIDR(t *testing.T) {
	ip := "255.0.0.7"
	n := 10
	actual := IpToCIDR(ip, n)
	expected := []string{"255.0.0.7/32", "255.0.0.8/29", "255.0.0.16/32"}
	assert.Equal(t, expected, actual)
}

func TestNumUniqueEmails(t *testing.T) {
	emails := []string{"test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com", "testemail+david@lee.tcode.com"}
	expected := 2
	actual := NumUniqueEmails(emails)
	assert.Equal(t, expected, actual)
}

func TestNumJewelsInStones(t *testing.T) {
	J, S := "aA", "aAAbbbb"
	expected := 3
	actual := NumJewelsInStones(J, S)
	assert.Equal(t, expected, actual)

	J, S = "z", "ZZ"
	expected = 0
	actual = NumJewelsInStones(J, S)
	assert.Equal(t, expected, actual)

}

func TestIsAlienSorted(t *testing.T) {
	words := []string{"hello", "leetcode"}
	order := "hlabcdefgijkmnopqrstuvwxyz"
	assert.True(t, IsAlienSorted(words, order))

	words = []string{"word", "world", "row"}
	order = "worldabcefghijkmnpqstuvxyz"
	assert.False(t, IsAlienSorted(words, order))

	words = []string{"apple", "app"}
	order = "abcdefghijklmnopqrstuvwxyz"
	assert.False(t, IsAlienSorted(words, order))
}

func TestPlusOne(t *testing.T) {
	digits := []int{4, 3, 2, 1}
	expected := []int{4, 3, 2, 2}
	actual := PlusOne(digits)
	assert.Equal(t, expected, actual)

	digits = []int{9, 9}
	expected = []int{1, 0, 0}
	actual = PlusOne(digits)
	assert.Equal(t, expected, actual)
}

func TestConvertToTitle(t *testing.T) {
	n := 26
	actual := convertToTitle(n)
	expected := "Z"

	assert.Equal(t, expected, actual)

	for i := 1; i < 900; i++ {
		println(i, "-", convertToTitle(i))
	}
}

func TestFlipAndInvertImage(t *testing.T) {
	A := [][]int{{1, 1, 0}, {1, 0, 1}, {0, 0, 0}}
	expected := [][]int{{1, 0, 0}, {0, 1, 0}, {1, 1, 1}}
	output := FlipAndInvertImage(A)
	assert.Equal(t, expected, output)

	A = [][]int{{1, 1, 0, 0}, {1, 0, 0, 1}, {0, 1, 1, 1}, {1, 0, 1, 0}}
	expected = [][]int{{1, 1, 0, 0}, {0, 1, 1, 0}, {0, 0, 0, 1}, {1, 0, 1, 0}}
	output = FlipAndInvertImage(A)
	assert.Equal(t, expected, output)
}

func TestPowerfulIntegers(t *testing.T) {
	x, y, bound := 2, 3, 10
	//expected := []int{2, 3, 4, 5, 7, 9, 10}
	actual := PowerfulIntegers(x, y, bound)
	//assert.Equal(t, expected, actual)
	assert.Equal(t, 7, len(actual))
}

func TestValidMountainArray(t *testing.T) {
	A := []int{0, 3, 2, 1}
	assert.True(t, ValidMountainArray(A))

	A = []int{2, 1}
	assert.False(t, ValidMountainArray(A))

	A = []int{3, 5, 5}
	assert.False(t, ValidMountainArray(A))

	A = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	assert.False(t, ValidMountainArray(A))
}

func TestRob(t *testing.T) {
	nums := []int{1, 2, 3, 1}
	expected := 4
	actual := Rob(nums)
	assert.Equal(t, expected, actual)

	nums = []int{2, 7, 9, 3, 1}
	expected = 12
	actual = Rob(nums)
	assert.Equal(t, expected, actual)

	nums = []int{}
	expected = 0
	actual = Rob(nums)
	assert.Equal(t, expected, actual)

}

func TestMinCost(t *testing.T) {
	costs := [][]int{{17, 2, 17}, {16, 16, 5}, {14, 3, 19}}
	expected := 10
	actual := MinCost(costs)
	assert.Equal(t, expected, actual)

	costs = [][]int{}
	expected = 0
	actual = MinCost(costs)
	assert.Equal(t, expected, actual)
}

func TestKClosest(t *testing.T) {
	points := [][]int{{1, 3}, {-2, 2}}
	K := 1
	expected := [][]int{{-2, 2}}
	actual := KClosest(points, K)
	assert.Equal(t, expected, actual)

	points = [][]int{{3, 3}, {5, -1}, {-2, 4}}
	K = 2
	expected = [][]int{{3, 3}, {-2, 4}}
	actual = KClosest(points, K)
	assert.Equal(t, expected, actual)

	points = [][]int{{-10000, -10000}, {10000, 10000}}
	K = 2
	expected = [][]int{{-10000, -10000}, {10000, 10000}}
	actual = KClosest(points, K)
	assert.Equal(t, expected, actual)
}

func TestLargestPerimeter(t *testing.T) {
	A := []int{2, 1, 2}
	expected := 5
	actual := LargestPerimeter(A)
	assert.Equal(t, expected, actual)

	A = []int{1, 2, 1}
	expected = 0
	actual = LargestPerimeter(A)
	assert.Equal(t, expected, actual)

	A = []int{3, 2, 3, 4}
	expected = 10
	actual = LargestPerimeter(A)
	assert.Equal(t, expected, actual)

	A = []int{3, 6, 2, 3}
	expected = 8
	actual = LargestPerimeter(A)
	assert.Equal(t, expected, actual)
}

func TestIsHappy(t *testing.T) {
	n := 19
	assert.True(t, IsHappy(n))

	n = 91
	assert.True(t, IsHappy(n))

	n = 1
	assert.True(t, IsHappy(n))

	n = 0
	assert.False(t, IsHappy(n))

	n = 9
	assert.False(t, IsHappy(n))

	n = 7
	assert.True(t, IsHappy(n))
}
