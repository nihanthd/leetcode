package easy

import (
	"fmt"
)

func lowestCommonAncestorBst(root, p, q *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}

	if root.Val == p.Val || root.Val == q.Val {
		return root
	}

	if p.Val > root.Val && q.Val > root.Val {
		return lowestCommonAncestorBst(root.Right, p, q)
	} else if p.Val < root.Val && q.Val < root.Val {
		return lowestCommonAncestorBst(root.Left, p, q)
	} else {
		return root
	}

}

func LowestCommonAncestorBST() {
	p := &TreeNode{
		Val:   5,
		Left:  nil,
		Right: nil,
	}

	q := &TreeNode{
		Val:   4,
		Left:  nil,
		Right: nil,
	}

	root := &TreeNode{
		Val: 6,
		Left: &TreeNode{
			Val: 2,
			Left: &TreeNode{
				Val:   0,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val: 4,
				Left: &TreeNode{
					Val:   3,
					Left:  nil,
					Right: nil,
				},
				Right: &TreeNode{
					Val:   5,
					Left:  nil,
					Right: nil,
				},
			},
		},
		Right: &TreeNode{
			Val: 8,
			Left: &TreeNode{
				Val:   7,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   9,
				Left:  nil,
				Right: nil,
			},
		},
	}

	lca := lowestCommonAncestorBst(root, p, q)
	fmt.Println(lca)
}
