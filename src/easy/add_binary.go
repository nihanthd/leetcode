package easy

//https://leetcode.com/problems/add-binary/description/

func addBinary(a string, b string) string {
	var length int
	if len(a) > len(b) {
		length = len(a)
	} else {
		length = len(b)
	}

	//Iterate from right to left
	//Logic compare characters at same index.1 & 1 = 0 + carry(1)
	//1 & 0 or 0 & 1, then 1 into sum
	// if there is no character at a particular index then assume that character to be 0
	var aByte, bByte, carryByte byte
	carryByte = '0'
	var solutionBytes []byte
	for i, j, pos := len(a)-1, len(b)-1, length-1; pos >= 0; pos, i, j = pos-1, i-1, j-1 {
		if i >= 0 {
			aByte = a[i]
		} else {
			aByte = '0'
		}
		if j >= 0 {
			bByte = b[j]
		} else {
			bByte = '0'
		}
		if aByte == bByte && aByte == '1' {
			if carryByte == '1' {
				solutionBytes = append([]byte{'1'}, solutionBytes...)
			} else {
				solutionBytes = append([]byte{'0'}, solutionBytes...)
			}
			carryByte = '1'
		} else if aByte == bByte && aByte == '0' { //This means both the bytes are 0
			if carryByte == '1' {
				solutionBytes = append([]byte{'1'}, solutionBytes...)
			} else {
				solutionBytes = append([]byte{'0'}, solutionBytes...)
			}
			carryByte = '0'
		} else {
			if carryByte == '1' {
				solutionBytes = append([]byte{'0'}, solutionBytes...)
				carryByte = '1'
			} else {
				solutionBytes = append([]byte{'1'}, solutionBytes...)
				carryByte = '0'
			}

		}
	}
	if carryByte == '1' {
		solutionBytes = append([]byte{'1'}, solutionBytes...)
	}

	return string(solutionBytes)
}
