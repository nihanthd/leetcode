package easy

/*
https://leetcode.com/problems/invert-binary-tree/
*/

func invertTree(root *TreeNode) *TreeNode {
	if root == nil {
		return root
	}
	left := root.Left
	root.Left = root.Right
	root.Right = left
	invertTree(root.Left)
	invertTree(root.Right)
	return root
}
