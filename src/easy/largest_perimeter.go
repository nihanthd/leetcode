package easy

import "sort"

func LargestPerimeter(A []int) int {
	perimeter := 0
	sort.Slice(A, func(i, j int) bool {
		return A[i] < A[j]
	})
	for i := len(A) - 1; i >= 2; i-- {
		a := A[i]
		b := A[i-1]
		c := A[i-2]
		if isAreaZero(a, b, c) {
			perimeter = a + b + c
			break
		}
	}
	return perimeter
}

func isAreaZero(a, b, c int) bool {
	if a+b <= c || a+c <= b || b+c <= a {
		return false
	}
	return true
}
