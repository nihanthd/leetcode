package easy

/*
https://leetcode.com/problems/plus-one/description/
*/

func PlusOne(digits []int) []int {
	digits[len(digits)-1] += 1
	carry := digits[len(digits)-1] / 10
	digits[len(digits)-1] = digits[len(digits)-1] % 10
	for i := len(digits) - 2; i >= 0 && carry > 0; i-- {
		digits[i] += carry
		carry = digits[i] / 10
		digits[i] = digits[i] % 10
	}

	if carry > 0 {
		digits = append([]int{carry}, digits...)
	}
	return digits
}
