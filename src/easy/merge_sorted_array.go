package easy

import (
	"fmt"
)

func merge(nums1 []int, m int, nums2 []int, n int) {
	i, j := 0, 0
	for i < m && j < n {
		if nums1[i] <= nums2[j] {
			i++
		} else {
			temp := nums1[i]
			nums1[i] = nums2[j]
			nums2[j] = temp
			//reorder nums2
			for k := j + 1; k < len(nums2); k++ {
				if nums2[k-1] > nums2[k] {
					x := nums2[k-1]
					nums2[k-1] = nums2[k]
					nums2[k] = x
				} else {
					break
				}
			}
			i++
		}
	}
	for ; j < n; j, i = j+1, i+1 {
		nums1[i] = nums2[j]
	}
	fmt.Println(nums1)
}

func MergeSortedArrays() {
	nums1 := []int{1, 2, 3, 0, 0, 0}
	m := 3
	nums2 := []int{4, 5, 6}
	n := 3
	merge(nums1, m, nums2, n)
}
