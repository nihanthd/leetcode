package easy

import (
	"fmt"
)

func twoSum(nums []int, target int) []int {
	numbers := make(map[int]int)
	for i, curNum := range nums {
		//if the number is found, then return the index of current number and
		//index of the number with target-curNum
		if index, ok := numbers[curNum]; ok {
			return []int{index, i}
		} else {
			numbers[target-curNum] = i
		}
	}
	return nil
}

func TwoSum() {
	x := twoSum([]int{2, 7, 11, 15}, 24)
	fmt.Printf("The indices where the solution exists = %v \n", x)
}
