package easy

import (
	"bytes"
	"fmt"
	"math"
)

func longestCommonPrefix(strs []string) string {
	var prefix string
	if len(strs) > 0 {
		prefix = strs[0]
	}
	for i := 1; i < len(strs) && len(prefix) > 0; i++ {
		var b bytes.Buffer

		//now iterate over both prefix and strs[i] to find the new prefix
		//and we can iterate untill the smallest string
		curStr := strs[i]
		maxLen := int(math.Min(float64(len(prefix)), float64(len(curStr))))
		for j := 0; j < maxLen && prefix[j] == curStr[j]; j++ {
			b.WriteByte(prefix[j])
		}
		prefix = b.String()
	}
	return prefix
}

func LongestCommonPrefix() {
	//input := []string{"flower", "flow", "flight"}
	input := []string{"dog", "racecar", "car"}
	prefix := longestCommonPrefix(input)
	fmt.Println("The longest common prefix '", prefix, "'")
}
