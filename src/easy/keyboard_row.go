package easy

import (
	"fmt"
	"strings"
)

func findWords(words []string) []string {
	var result []string
	keyBoardRows := map[byte]int{
		'q': 1, 'w': 1, 'e': 1, 'r': 1,
		't': 1, 'y': 1, 'u': 1, 'i': 1,
		'o': 1, 'p': 1, 'a': 2, 's': 2,
		'd': 2, 'f': 2, 'g': 2, 'h': 2,
		'j': 2, 'k': 2, 'l': 2, 'z': 3,
		'x': 3, 'c': 3, 'v': 3, 'b': 3,
		'n': 3, 'm': 3,
	}
	for _, word := range words {
		row := 0
		belongsToSingleRow := true
		for _, char := range word {
			//assign row
			if row == 0 {
				row = keyBoardRows[[]byte(strings.ToLower(string(char)))[0]]
			} else if row != keyBoardRows[[]byte(strings.ToLower(string(char)))[0]] {
				belongsToSingleRow = false
				break
			}
		}
		if belongsToSingleRow {
			result = append(result, word)
		}
	}
	return result
}

func KeyboardRow() {
	input := []string{"Aasdfghjkl", "Qwertyuiop", "zZxcvbnm"}
	result := findWords(input)
	fmt.Printf("The words that belong to same keyboard row are : %v\n", result)
}
