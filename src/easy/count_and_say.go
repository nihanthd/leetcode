package easy

import (
	"bytes"
	"fmt"
	"strconv"
)

func countAndSay(n int) string {
	var current bytes.Buffer
	current.WriteByte('1')
	var previous string
	var count int64
	var char byte
	for i := 1; i < n; i++ {
		previous = current.String()
		current = bytes.Buffer{}
		count = 1
		char = previous[0]
		for j := 1; j < len(previous); j++ {
			if previous[j] != char {
				current.WriteString(strconv.FormatInt(count, 10))
				current.WriteByte(char)
				char = previous[j]
				count = 1
			} else {
				count++
			}
		}
		current.WriteString(strconv.FormatInt(count, 10))
		current.WriteByte(char)
	}
	return current.String()
}

func CountAndSay() {
	input := 30
	output := countAndSay(input)
	fmt.Printf("The count and say combination for %d is %s", input, output)
}
