package easy

import (
	"fmt"
	"math"
)

func maxDepth(root *TreeNode) int {
	var depth int
	if root == nil {
		return 0
	}
	depth++
	leftDepth := maxDepth(root.Left)
	rightDepth := maxDepth(root.Right)
	if leftDepth == 0 {
		return depth + rightDepth
	}
	if rightDepth == 0 {
		return depth + leftDepth
	}
	depth = depth + int(math.Max(float64(leftDepth), float64(rightDepth)))
	return depth
}

func MaximumDepthOfBinaryTree() {
	root := &TreeNode{
		Val: 3,
		Left: &TreeNode{
			Val:   9,
			Left:  nil,
			Right: nil,
		},
		Right: &TreeNode{
			Val: 20,
			Left: &TreeNode{
				Val:   15,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   7,
				Left:  nil,
				Right: nil,
			},
		},
	}
	//root := &TreeNode{
	//	Val: 3,
	//	Left: &TreeNode{
	//		Val: 3,
	//	},
	//	Right: nil,
	//}
	depth := maxDepth(root)
	fmt.Printf("The maximum depth of the tree is %d\n", depth)
}
