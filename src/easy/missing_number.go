package easy

import "fmt"

type missingNumber struct {
}

type MissingNumber interface {
	findMissingNumber()
}

func (m *missingNumber) findMissingNumber(nums []int) int {
	n := len(nums)
	var actualSum, expectedSum int
	expectedSum = (n * (n + 1)) / 2
	for _, val := range nums {
		actualSum += val
	}

	return expectedSum - actualSum
}

func NewMissingNumber() {
	m := missingNumber{}
	x := m.findMissingNumber([]int{9, 6, 4, 2, 3, 5, 7, 0, 1})

	fmt.Println(x)
}
