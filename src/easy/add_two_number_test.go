package easy

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddTwoNumber(t *testing.T) {
	l1 := &ListNode{
		Val: 2,
		Next: &ListNode{
			Val: 4,
			Next: &ListNode{
				Val:  8,
				Next: nil,
			},
		},
	}

	l2 := &ListNode{
		Val: 5,
		Next: &ListNode{
			Val: 6,
			Next: &ListNode{
				Val:  4,
				Next: nil,
			},
		},
	}
	output := addTwoNumbers(l1, l2)
	expectedOutput := &ListNode{
		Val: 7,
		Next: &ListNode{
			Val: 0,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val:  1,
					Next: nil,
				},
			},
		},
	}

	assert.Equal(t, expectedOutput, output, "")
}
