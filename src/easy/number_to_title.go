package easy

/*
https://leetcode.com/problems/excel-sheet-column-title/
*/
func convertToTitle(n int) string {
	var buff []byte
	for n > 0 {
		rem := n % 26
		n = n / 26
		if rem == 0 {
			rem = 26
			n = n - 1
		}
		buff = append([]byte{byte(rem + 64)}, buff...)

	}
	return string(buff)
}
