package easy

import (
	"fmt"
)

/*
https://leetcode.com/problems/valid-palindrome-ii/description/
*/

func validPalindrome(s string) bool {
	length := len(s)
	for i := 0; i < length/2; i++ {
		if s[i] != s[length-i-1] {
			j := length - i - 1
			return isPallindrome(s, i+1, j) || isPallindrome(s, i, j-1)
		}
	}
	return true
}

func isPallindrome(s string, i, j int) bool {
	for k := i; k <= i+((j-i)/2); k++ {
		if s[k] != s[j-k+i] {
			return false
		}
	}
	return true
}

func ValidPalindromeII() {
	input := "aguokepatgbnvfqmgmlcupuufxoohdfpgjdmysgvhmvffcnqxjjxqncffvmhvgsymdjgpfdhooxfuupuculmgmqfvnbgtapekouga"
	output := validPalindrome(input)
	fmt.Printf("%s is pallindrome? %t\n", input, output)
}
