package hard

import (
	"fmt"
)

/*

 */
func isWildcardMatch(s string, p string) bool {
	//rows is string 's'
	//fmt.Printf("\nString = '%s' pattern = '%s'\n\n", s, p)
	solution := make([][]bool, len(s)+1)
	//columns are for pattern 'p'
	for index, _ := range solution {
		solution[index] = make([]bool, len(p)+1)
	}
	solution[0][0] = true
	//Now we need to take care of patterns like *abc, *a*b, *a*b*c*
	for i := 1; i < len(solution[0]); i++ {
		if p[i-1] == '*' {
			solution[0][i] = solution[0][i-1]
		}
	}
	//printSolution(solution)

	//For each character in the string see if the pattern matches.

	for i := 1; i < len(solution); i++ {
		for j := 1; j < len(solution[0]); j++ {
			if s[i-1] == p[j-1] || p[j-1] == '?' {
				solution[i][j] = solution[i-1][j-1]
			} else if p[j-1] == '*' {
				solution[i][j] = solution[i-1][j] || solution[i][j-1]
			} else {
				solution[i][j] = false
			}
		}
	}
	return solution[len(s)][len(p)]
}

func printSolution(solution [][]bool) {
	for _, val := range solution {
		fmt.Println(val)
	}
	fmt.Println()
	fmt.Println("------------------")
	fmt.Println()
}
