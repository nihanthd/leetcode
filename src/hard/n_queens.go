package hard

import "fmt"

func solveNQueens(n int) [][]string {
	var solution [][]string
	for i := 0; i < n; i++ {
		var subSolution []string
		for j := 0; j < n; j++ {
			subSolution = append(subSolution, "")
		}
		solution = append(solution, subSolution)
	}
	//fillHorizontal(solution, 6, 6)
	//fillDiagonal1(solution, 6, 6)
	//fillDiagonal2(solution, 6, 6)
	fillHorizontal(solution, 6, 2)
	fillVertical(solution, 6, 2)
	fillDiagonal1(solution, 6, 2)
	fillDiagonal2(solution, 6, 2)

	//iterate over row
	return solution
}

func fillHorizontal(solution [][]string, row, col int) {
	rowToFill := solution[row]
	for i := 0; i < len(rowToFill); i++ {
		if i != col {
			rowToFill[i] = "."
		} else {
			rowToFill[i] = "Q"
		}
	}
}

func fillVertical(solution [][]string, row, col int) {
	for i := 0; i < len(solution); i++ {
		if i != row {
			solution[i][col] = "."
		} else {
			solution[i][col] = "Q"
		}
	}
}

func fillDiagonal1(solution [][]string, row, col int) bool {
	n := len(solution)
	for i, j := row+1, col+1; i < n; i, j = i+1, j+1 {
		solution[i][j] = "."

	}

	for i, j := row-1, col-1; j >= 0; i, j = i-1, j-1 {
		solution[i][j] = "."
	}

	return false
}

func fillDiagonal2(solution [][]string, row, col int) bool {
	n := len(solution)
	for i, j := row+1, col-1; i < n && j >= 0; i, j = i+1, j-1 {
		solution[i][j] = "."

	}

	for i, j := row-1, col+1; j < n && i >= 0; i, j = i-1, j+1 {
		solution[i][j] = "."
	}

	return false
}

func SolveNQueens() {
	result := solveNQueens(8)
	for _, val := range result {
		for _, subVal := range val {
			if subVal == "Q" {
				fmt.Print(subVal, "\t")
			} else {
				fmt.Print(".", "\t")
			}
		}
		fmt.Println("")
	}

}
