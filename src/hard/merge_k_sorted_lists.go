package hard

import "fmt"

func mergeKLists(lists []*ListNode) *ListNode {
	var result *ListNode
	if len(lists) == 0 {
		return result
	} else if len(lists) == 1 {
		return lists[0]
	} else {
		for len(lists) != 1 {
			var results []*ListNode
			for i := 0; i < len(lists); i += 2 {
				if i+1 < len(lists) {
					result = merge2Lists(lists[i], lists[i+1])
				} else {
					result = lists[i]
				}
				results = append(results, result)
			}
			lists = results
		}
	}
	return result
}

func merge2Lists(l1 *ListNode, l2 *ListNode) *ListNode {
	var head, node *ListNode
	for l1 != nil || l2 != nil {

		var val *ListNode

		if (l1 != nil && l2 != nil && l1.Val <= l2.Val) || l2 == nil {
			val = l1
			if l1 != nil {
				l1 = l1.Next
			}
		} else if l2 != nil || l1 == nil {
			val = l2
			if l2 != nil {
				l2 = l2.Next
			}
		}
		if node == nil {
			node = val
			head = node
		} else {
			node.Next = val
			node = node.Next
		}
	}
	return head
}

func MergeKLists() {
	lists := []*ListNode{
		{
			Val: 1,
			Next: &ListNode{
				Val: 4,
				Next: &ListNode{
					Val:  5,
					Next: nil,
				},
			},
		},
		{
			Val: 1,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val:  4,
					Next: nil,
				},
			},
		},
		{
			Val: 2,
			Next: &ListNode{
				Val:  6,
				Next: nil,
			},
		},
	}
	result := mergeKLists(lists)
	for result != nil {
		fmt.Println(result.Val)
		result = result.Next
	}
}
