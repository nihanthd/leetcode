package hard

import (
	"strings"
)

func NumberToWords(num int) string {
	thousands := []string{"", "Thousand", "Million", "Billion"}
	if num == 0 {
		return "Zero"
	}

	result := ""
	i := 0
	for num > 0 {
		if num%1000 != 0 {
			result = helper(num%1000) + thousands[i] + " " + result
		}
		num /= 1000
		i++
	}
	return strings.TrimSpace(result)
}

func helper(num int) string {
	lessThanTwenty := []string{"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"}
	tens := []string{"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"}
	if num == 0 {
		return ""
	} else if num < 20 {
		return lessThanTwenty[num] + " "
	} else if num < 100 {
		return tens[num/10] + " " + helper(num%10)
	} else {
		return lessThanTwenty[num/100] + " Hundred " + helper(num%100)
	}
}
