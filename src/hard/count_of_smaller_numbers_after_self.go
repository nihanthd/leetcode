package hard

func countSmaller(nums []int) []int {
	var root *Node
	result := make([]int, len(nums))
	for i := len(nums) - 1; i >= 0; i-- {
		root = insert(nums[i], root, &result, i, 0)
	}
	return result
}

func insert(number int, root *Node, result *[]int, i int, preSum int) *Node {
	if root == nil {
		root = &Node{
			Val:       number,
			Sum:       0,
			Duplicate: 1,
		}
		(*result)[i] = preSum
	} else if root.Val == number {
		root.Duplicate = root.Duplicate + 1
		(*result)[i] = preSum + root.Sum
	} else if root.Val > number {
		root.Sum = root.Sum + 1
		root.Left = insert(number, root.Left, result, i, preSum)
	} else {
		root.Right = insert(number, root.Right, result, i, preSum+root.Duplicate+root.Sum)
	}
	return root
}

type Node struct {
	Val       int
	Sum       int
	Duplicate int
	Left      *Node
	Right     *Node
}
