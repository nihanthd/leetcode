package hard

import (
	"fmt"
)

func isMatch(s string, p string) bool {
	//rows are for s and columns are for p
	solution := make([][]bool, len(s)+1)
	for i := 0; i < len(s)+1; i++ {
		solution[i] = make([]bool, len(p)+1)
	}
	solution[0][0] = true

	//this will help us in dealing with a* or b* or a*c*
	for i := 1; i < len(solution[0]); i++ {
		if p[i-1] == '*' {
			solution[0][i] = solution[0][i-2]
		}
	}

	//Now for each row and column check whether it is a valid pattern match so far.
	for i := 1; i < len(solution); i++ {
		for j := 1; j < len(solution[0]); j++ {
			//if it is a '.' or equal to the pattern then it takes the diagonal result
			if p[j-1] == '.' || p[j-1] == s[i-1] {
				solution[i][j] = solution[i-1][j-1]
			} else if p[j-1] == '*' {
				//if it is a '*' then certainly it will be same as 0 matches of the previous character so j-2
				solution[i][j] = solution[i][j-2]
				//if it is a '.' or exact match of pattern then
				if p[j-2] == '.' || p[j-2] == s[i-1] {
					solution[i][j] = solution[i][j] || solution[i-1][j]
				}
			} else {
				solution[i][j] = false
			}
		}
	}
	return solution[len(s)][len(p)]
}

func RegularExpressionMatching() {
	input := "abc"
	regex := "a*.*"
	didItMatch := isMatch(input, regex)
	fmt.Println("Did the regex match the input ? ", didItMatch)
}
