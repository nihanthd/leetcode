package hard

import (
	"container/heap"
	"math"
)

type Item struct {
	Cost  int
	Stop  int
	Place int
}

type PriorityQueue []Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].Cost < pq[j].Cost
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PriorityQueue) Push(x interface{}) {
	*pq = append(*pq, x.(Item))
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	*pq = old[0 : n-1]
	return item
}

func FindCheapestPrice(n int, flights [][]int, src int, dst int, K int) int {
	graph := make([][]int, n)
	for i := 0; i < n; i++ {
		graph[i] = make([]int, n)
	}
	for _, flight := range flights {
		graph[flight[0]][flight[1]] = flight[2]
	}
	best := make(map[int]int)
	pq := make(PriorityQueue, 0)
	heap.Init(&pq)
	heap.Push(&pq, Item{0, 0, src})

	for len(pq) != 0 {
		info := heap.Pop(&pq).(Item)
		cost := info.Cost
		stop := info.Stop
		place := info.Place
		bestCost, ok := best[stop*1000+place]
		if !ok {
			bestCost = math.MaxInt32
		}
		if stop > K+1 || cost > bestCost {
			continue
		}
		if place == dst {
			return cost
		}
		for i := 0; i < n; i++ {
			if graph[place][i] > 0 {
				newCost := cost + graph[place][i]
				bestSubCost, subOk := best[(stop+1)*1000+i]
				if !subOk {
					bestSubCost = math.MaxInt32
				}
				if newCost < bestSubCost {
					heap.Push(&pq, Item{newCost, stop + 1, i})
					best[(stop+1)*1000+i] = newCost
				}
			}
		}
	}
	return -1
}
