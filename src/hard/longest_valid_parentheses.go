package hard

import "fmt"

func longestValidParentheses(s string) int {
	var length int
	var stack []int
	stack = append(stack, -1)
	for index := 0; index < len(s); index++ {
		if s[index] == '(' {
			stack = append(stack, index)
		} else {
			stack = stack[0 : len(stack)-1]
			if len(stack) == 0 {
				stack = append(stack, index)
			} else {
				length = max(length, index-stack[len(stack)-1])
			}

		}
	}
	return length
}

func LongestValidParentheses() {
	input := "())((())"
	input = "((())"
	input = ")()"
	output := longestValidParentheses(input)
	fmt.Printf("Longest Valid Parantheses is %d\n", output)
}
