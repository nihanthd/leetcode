package hard

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMaxSlidingWindow(t *testing.T) {
	k := 3
	nums := []int{1, 3, -1, -3, 5, 3, 6, 7}
	output := maxSlidingWindow(nums, k)
	expected := []int{3, 3, 5, 5, 6, 7}
	assert.Equal(t, expected, output)
}

func TestRegularExpressionMatching(t *testing.T) {
	RegularExpressionMatching()
}

func TestWildCardMatching(t *testing.T) {
	expected := true
	output := isWildcardMatch("abc", "*b*")
	assert.Equal(t, expected, output)
}

func TestCountSmaller(t *testing.T) {
	input := []int{5, 4, 3, 2, 1}
	output := countSmaller(input)
	expected := []int{4, 3, 2, 1, 0}
	assert.Equal(t, expected, output)
}

func TestFindKthSmallestInLexicographicalOrder(t *testing.T) {
	n, k := 13, 2
	output := findKthNumber(n, k)
	expected := 10
	assert.Equal(t, expected, output)

	k = 7
	output = findKthNumber(n, k)
	expected = 3
	assert.Equal(t, expected, output)
}

func TestNumberToWords(t *testing.T) {
	input := 1000
	actual := NumberToWords(input)
	expected := "One Thousand"

	assert.Equal(t, expected, actual)

	input = 1268
	actual = NumberToWords(input)
	expected = "One Thousand Two Hundred Sixty Eight"
	assert.Equal(t, expected, actual)

	input = 648
	actual = NumberToWords(input)
	expected = "Six Hundred Forty Eight"
	assert.Equal(t, expected, actual)
}

func TestAlienOrder(t *testing.T) {
	words := []string{"wrt", "wrf", "er", "ett", "rftt"}
	actual := AlienOrder(words)
	expected := "wertf"
	assert.Equal(t, expected, actual)

	words = []string{"z", "x", "z"}
	actual = AlienOrder(words)
	expected = ""
	assert.Equal(t, expected, actual)

	words = []string{"z", "x"}
	actual = AlienOrder(words)
	expected = "zx"
	assert.Equal(t, expected, actual)
}

func TestFindCheapestPrice(t *testing.T) {
	n, src, dst, stops := 3, 0, 2, 1
	flights := [][]int{
		{0, 1, 100},
		{1, 2, 100},
		{0, 2, 500},
	}

	expected := 200
	actual := FindCheapestPrice(n, flights, src, dst, stops)
	assert.Equal(t, expected, actual)

	n, src, dst, stops = 10, 6, 0, 7
	flights = [][]int{
		{3, 4, 4}, {2, 5, 6}, {4, 7, 10}, {9, 6, 5}, {7, 4, 4}, {6, 2, 10},
		{6, 8, 6}, {7, 9, 4}, {1, 5, 4}, {1, 0, 4}, {9, 7, 3}, {7, 0, 5},
		{6, 5, 8}, {1, 7, 6}, {4, 0, 9}, {5, 9, 1}, {8, 7, 3}, {1, 2, 6},
		{4, 1, 5}, {5, 2, 4}, {1, 9, 1}, {7, 8, 10}, {0, 4, 2}, {7, 2, 8},
	}

	expected = 14
	actual = FindCheapestPrice(n, flights, src, dst, stops)
	assert.Equal(t, expected, actual)
}

//func TestMinDistance(t *testing.T) {
//	word1 := "ABC"
//	word2 := "BC"
//	fmt.Println(minDistance(word1, word2))
//}
