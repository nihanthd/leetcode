package hard

/*
124 - https://leetcode.com/problems/binary-tree-maximum-path-sum/
*/

func MaxPathSum(root *TreeNode) int {
	max, _, _ := maximumSumForAPath(root)
	return max
}

func maximumSumForAPath(root *TreeNode) (int, int, int) {
	if root == nil {
		return 0, 0, 0
	}
	//maxLeft, leftPathLeft, leftPathRight := maximumSumForAPath(root.Left)
	//maxRight, rightPathLeft, rightPathRight := maximumSumForAPath(root.Right)

	return 0, 0, 0

}
