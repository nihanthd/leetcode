package hard

/*
https://leetcode.com/problems/k-th-smallest-in-lexicographical-order/
*/
//It is like a denary tree
func findKthNumber(n int, k int) int {
	current := 1
	//since we have already taken a step by initializing current with 1
	k -= 1
	for k > 0 {
		//We find the number of steps that are needed to reach the kth number
		steps := calSteps(n, current, current+1)
		if steps <= k {
			//that means the number needs to be increased and number of steps moved have to be decreased.
			current += 1
			k -= steps
		} else {
			//we are taking a leap of ten numbers so k will only decrease by 1
			current *= 10
			k -= 1
		}
	}
	return current
}

func calSteps(n, n1, n2 int) int {
	steps := 0
	//to make sure we move the number of steps that are required to get the number
	for n1 <= n {
		//this will get us the number of steps
		steps += min(n+1, n2) - n1
		n1 *= 10
		n2 *= 10
	}
	return steps
}
