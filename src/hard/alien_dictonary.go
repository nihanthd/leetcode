package hard

func AlienOrder(words []string) string {
	graph := buildGraph(words)
	return topologicalSort(graph)
}

func topologicalSort(graph []*GraphNode) string {
	var stack []byte
	visited := make(map[*GraphNode]bool)
	for _, node := range graph {
		if _, ok := visited[node]; ok {
			continue
		}
		isCycle := topSortUtil(node, &stack, &visited)
		if isCycle {
			return ""
		}
	}
	return string(stack)
}

func topSortUtil(node *GraphNode, stack *[]byte, visited *map[*GraphNode]bool) bool {
	(*visited)[node] = true
	for _, child := range node.AdjList {
		if _, ok := (*visited)[child]; ok {
			return true
		}
		if topSortUtil(child, stack, visited) {
			return true
		}
	}
	*stack = append([]byte{node.Value}, *stack...)
	return false
}

func buildGraph(words []string) []*GraphNode {
	//creates the nodes of the graph to easily identify the nodes later when building the graph
	var graph []*GraphNode
	graphNodeSet := make(map[byte]*GraphNode)
	for _, word := range words {
		//each character in the word represents a node in the graph.
		for i := 0; i < len(word); i++ {
			if _, ok := graphNodeSet[word[i]]; !ok {
				graphNode := &GraphNode{
					Value: word[i],
				}
				graph = append(graph, graphNode)
				graphNodeSet[word[i]] = graphNode
			}
		}
	}
	//Let us also make sure that the same edge is not created twice
	//Now for each adjacent word if there is a character mismatch then add the characters into the adjacency list
	for i := 0; i < len(words)-1; i++ {
		word1 := words[i]
		word2 := words[i+1]
		maxIterate := min(len(word1), len(word2))
		for j := 0; j < maxIterate; j++ {
			//the byte at j is not the same in word1 and word2
			if word1[j] != word2[j] {
				//Now let us add the adjacency list dependency from byte in word1 to word2
				found := false
				word2Node := graphNodeSet[word2[j]]
				word1Node := graphNodeSet[word1[j]]
				for _, val := range word1Node.AdjList {
					if val == word2Node {
						found = true
						break
					}
				}

				if !found {
					graphNodeSet[word1[j]].AdjList = append(graphNodeSet[word1[j]].AdjList, word2Node)
				}
			}
		}
	}
	return graph
}

type GraphNode struct {
	Visited bool
	Value   byte
	AdjList []*GraphNode
}
