package hard

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}
