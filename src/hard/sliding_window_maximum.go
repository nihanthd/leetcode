package hard

func maxSlidingWindow(nums []int, k int) []int {
	var result []int
	deQueue := []int{}
	if len(nums) < k || len(nums) == 0 {
		return []int{}
	}
	i := 0
	for ; i < k; i++ {
		//remove element's indexes which are smaller than the current element
		for len(deQueue) != 0 && nums[i] >= nums[deQueue[len(deQueue)-1]] {
			deQueue = deQueue[:len(deQueue)-1]
		}
		deQueue = append(deQueue, i)
	}

	for ; i < len(nums); i++ {
		//element at the first position is the biggest element
		result = append(result, nums[deQueue[0]])

		//now remove the elements which are out of the next sliding window
		for len(deQueue) != 0 && deQueue[0] <= i-k {
			deQueue = deQueue[1:]
		}

		//add the element to the last by removing element's which are smaller than the element being inserted.
		for len(deQueue) != 0 && nums[i] >= nums[deQueue[len(deQueue)-1]] {
			deQueue = deQueue[:len(deQueue)-1]
		}
		deQueue = append(deQueue, i)
	}
	result = append(result, nums[deQueue[0]])
	return result
}
