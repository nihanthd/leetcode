package backtracking

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFindWords(t *testing.T) {
	board := [][]byte{{'o', 'a', 'a', 'n'}, {'e', 't', 'a', 'e'}, {'i', 'h', 'k', 'r'}, {'i', 'f', 'l', 'v'}}
	words := []string{"oath", "pea", "eat", "rain"}
	expected := []string{"eat", "oath"}
	actual := FindWords(board, words)
	assert.Equal(t, expected, actual)

	board = [][]byte{{'a'}}
	words = []string{"a", "a"}
	expected = []string{"a"}
	actual = FindWords(board, words)
	assert.Equal(t, expected, actual)
}
