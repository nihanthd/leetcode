package backtracking

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSearchWordInMatrix(t *testing.T) {
	input := [][]byte{{'t', 'z', 'x', 'c', 'd'},
		{'a', 'h', 'n', 'z', 'x'}, {'h', 'w', 'o', 'i', 'o'},
		{'o', 'r', 'n', 'r', 'n'}, {'a', 'b', 'r', 'i', 'n'}}
	var solution [][]int
	for i := 0; i < len(input); i++ {
		solution = append(solution, make([]int, len(input[i])))
		//solution[i] = append(solution[i], make([]int, len(matrix[i]))...)
	}
	inputWord := "horizon"
	found := searchWord(input, solution, inputWord)
	expectedFound := true
	assert.Equal(t, expectedFound, found, "")

	inputWord = "horizontal"
	found = searchWord(input, solution, inputWord)
	expectedFound = false
	assert.Equal(t, expectedFound, found, "")
}
