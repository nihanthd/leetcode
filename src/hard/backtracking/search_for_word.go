package backtracking

import (
	"fmt"
)

/*
https://algorithms.tutorialhorizon.com/backtracking-search-a-word-in-a-matrix/
*/

var Path = 1

func searchWord(matrix [][]byte, solution [][]int, input string) bool {
	maxRows := len(matrix)
	for i := 0; i < maxRows; i++ {
		maxColumns := len(matrix[i])
		for j := 0; j < maxColumns; j++ {
			index := 0
			if searchInNeighbourHood(matrix, solution, i, j, index, maxRows, maxColumns, input) {
				return true
			}
		}
	}
	return false
}

func searchInNeighbourHood(matrix [][]byte, solution [][]int, row, col, index, maxRows, maxColumns int, input string) bool {

	//if at any point we have crossed searching until the last index, that means we have found a solution
	if index >= len(input) {
		return true
	}

	//if it was pre used or else the input[index] is not equal to the character
	if solution[row][col] != 0 || input[index] != matrix[row][col] {
		return false
	}

	solution[row][col] = Path
	Path++
	index++

	//Now we have eight directions to go to find out if we find a match

	//go →, i.e. increase column in same row
	if col < maxColumns-1 && searchInNeighbourHood(matrix, solution, row, col+1, index, maxRows, maxColumns, input) {
		return true
	}
	//go ←, i.e. decrease column in same row
	if col > 0 && searchInNeighbourHood(matrix, solution, row, col-1, index, maxRows, maxColumns, input) {
		return true
	}

	//go ↑, i.e. decrease the row in same column
	if row > 0 && searchInNeighbourHood(matrix, solution, row-1, col, index, maxRows, maxColumns, input) {
		return true
	}

	//go ↓, i.e. increase the row in same column
	if row < maxRows-1 && searchInNeighbourHood(matrix, solution, row+1, col, index, maxRows, maxColumns, input) {
		return true
	}

	//go ↖, i.e. decrease the row and decrease the column
	if row > 0 && col > 0 && searchInNeighbourHood(matrix, solution, row-1, col-1, index, maxRows, maxColumns, input) {
		return true
	}

	//go ↙, i.e. increase the row and decrease the column
	if row < maxRows-1 && col > 0 && searchInNeighbourHood(matrix, solution, row+1, col-1, index, maxRows, maxColumns, input) {
		return true
	}

	//go ↗, i.e. decrease the row and increase the column
	if row > 0 && col < maxColumns-1 && searchInNeighbourHood(matrix, solution, row-1, col+1, index, maxRows, maxColumns, input) {
		return true
	}
	//go ↘, i.e. increase the row and increase the column
	if row < maxRows-1 && col < maxColumns-1 && searchInNeighbourHood(matrix, solution, row+1, col+1, index, maxRows, maxColumns, input) {
		return true
	}

	//If we have not found the solution any where in the eight directions then we have to back track and decrease path

	solution[row][col] = 0
	Path--
	index--
	return false
}

func SearchWordInMatrix() {
	matrix := [][]byte{{'t', 'z', 'x', 'c', 'd'},
		{'a', 'h', 'n', 'z', 'x'}, {'h', 'w', 'o', 'i', 'o'},
		{'o', 'r', 'n', 'r', 'n'}, {'a', 'b', 'r', 'i', 'n'}}
	var solution [][]int
	for i := 0; i < len(matrix); i++ {
		solution = append(solution, make([]int, len(matrix[i])))
		//solution[i] = append(solution[i], make([]int, len(matrix[i]))...)
	}
	input := "horizon"
	found := searchWord(matrix, solution, input)
	if found {
		printSolution(solution)
	} else {
		fmt.Println("There is no solution.")
	}
}

func printSolution(solution [][]int) {
	for _, subSolution := range solution {
		for _, val := range subSolution {
			fmt.Printf("%d \t", val)
		}
		fmt.Println()
	}
}
