package medium

func numIslands(grid [][]byte) int {
	//we are taking 2 as if it is 0 and at the last we subtract 2
	//'2' is used to mark the island, so that
	islandCount := 0
	//Now iterate over each cell in the grid and mark it
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[i]); j++ {
			if grid[i][j] == '1' {
				grid[i][j] = '2'
				islandCount++
				markAdjacentPortionsOfIsland(grid, i, j)
			}
		}
	}
	return islandCount
}

func markAdjacentPortionsOfIsland(grid [][]byte, i, j int) {
	//is the ← cell of (i,j) an Island not marked, then mark it with '2'
	if i >= 0 && j-1 >= 0 && grid[i][j-1] == '1' {
		grid[i][j-1] = '2'
		markAdjacentPortionsOfIsland(grid, i, j-1)
	}

	//is the ↑ cell of (i,j) an Island not marked, then mark it with '2'
	if i-1 >= 0 && j >= 0 && grid[i-1][j] == '1' {
		grid[i-1][j] = '2'
		markAdjacentPortionsOfIsland(grid, i-1, j)
	}

	//is the → cell of (i,j) an Island not marked, then mark it with '2'
	if i < len(grid) && j+1 < len(grid[i]) && grid[i][j+1] == '1' {
		grid[i][j+1] = '2'
		markAdjacentPortionsOfIsland(grid, i, j+1)
	}

	if i+1 < len(grid) && j < len(grid[i+1]) && grid[i+1][j] == '1' {
		grid[i+1][j] = '2'
		markAdjacentPortionsOfIsland(grid, i+1, j)
	}
}

func isAdjacentPartOfAnIsland(grid [][]byte, i, j int) bool {
	//is the ← cell an Island
	if j-1 >= 0 && grid[i][j-1] > '1' {
		return true
	}

	//is the ↑ cell an Island
	if i-1 >= 0 && grid[i-1][j] > '1' {
		return true
	}

	return false
}
