package medium

import (
	"math"
)

func coinChange(coins []int, amount int) int {
	if amount < 1 {
		return 0
	}
	return coinChanges(coins, amount, make([]int, amount))
}

func coinChanges(coins []int, rem int, count []int) int {
	if rem < 0 || len(coins) == 0 {
		return -1
	}
	if rem == 0 {
		return 0
	}
	if count[rem-1] != 0 {
		return count[rem-1]
	}
	minResult := math.MaxInt32
	for _, val := range coins {
		tempResult := coinChanges(coins, rem-val, count) + 1
		if tempResult > 0 && tempResult < minResult {
			minResult = tempResult
		}
	}
	count[rem-1] = minResult
	if minResult == math.MaxInt32 {
		count[rem-1] = -1
	}
	return count[rem-1]
}
