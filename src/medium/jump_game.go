package medium

func CanJump(nums []int) bool {
	return canJumpIndex(nums, 0)
}

func canJumpIndex(nums []int, index int) bool {
	if index == len(nums)-1 {
		return true
	}

	maxJump := min(index+nums[index], len(nums)-1)
	for i := maxJump; i > index; i-- {
		if canJumpIndex(nums, i) {
			return true
		}
	}

	return false
}
