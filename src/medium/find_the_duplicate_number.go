package medium

/*
https://leetcode.com/problems/find-the-duplicate-number/description/
*/
func findDuplicate(nums []int) int {
	tortoise := nums[0]
	hare := nums[0]
	for {
		tortoise = nums[tortoise]
		hare = nums[nums[hare]]
		if tortoise == hare {
			break
		}
	}

	// Find the "entrance" to the cycle.
	num1 := nums[0]
	num2 := tortoise
	for num1 != num2 {
		num1 = nums[num1]
		num2 = nums[num2]
	}

	return num1
}
