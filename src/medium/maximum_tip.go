package medium

import (
	"fmt"
	"math"
)

func findMaximumTipRecursive(a, b []int, max, index, x, y int) int {
	var tip int
	if index >= max || x < 0 || y < 0 {
		return 0
	}
	if x == 0 && index < max {
		tip = b[index] + findMaximumTipRecursive(a, b, max, index+1, x, y-1)
	} else if y == 0 && index < max {
		tip = a[index] + findMaximumTipRecursive(a, b, max, index+1, x-1, y)
	} else {
		tip = int(math.Max(float64(a[index]+findMaximumTipRecursive(a, b, max, index+1, x-1, y)), float64(b[index]+findMaximumTipRecursive(a, b, max, index+1, x, y-1))))
	}
	return tip
}

func FindMaximumTipRecursive() {
	max := 5
	x := 3
	y := 3
	a := []int{1, 2, 3, 4, 5}
	b := []int{5, 4, 3, 2, 1}

	tip := findMaximumTipRecursive(a, b, max, 0, x, y)
	fmt.Println("Maximum Tip Recursive = ", tip)

	tip = findMaximumTipIterative(a, b, max, 0, x, y)
	fmt.Println("Maximum Tip Iterative = ", tip)
}

func findMaximumTipIterative(a, b []int, max, index, x, y int) int {
	var tip int

	differences := findDifferences(a, b)
	indexes := reorderByDifference(differences)
	for _, val := range indexes {
		if index < max {
			if a[val] > b[val] {
				if x > 0 {
					tip += a[val]
					x -= 1
				} else {
					tip += b[val]
					y -= 1
				}
			} else {
				if y > 0 {
					tip += b[val]
					y -= 1
				} else {
					tip += a[val]
					x -= 1
				}
			}
			index += 1
		}
	}
	return tip
}

func findDifferences(a, b []int) []int {
	var differences []int
	for i := 0; i < len(a); i++ {
		differences = append(differences, int(math.Abs(float64(a[i]-b[i]))))
	}
	return differences
}

func reorderByDifference(differences []int) []int {
	var indexes []int
	for i := 0; i < len(differences); i++ {
		indexes = append(indexes, i)
	}
	for i := 0; i < len(differences)-1; i++ {
		for j := 0; j < len(differences)-1; j++ {
			if differences[j] < differences[j+1] {
				temp := differences[j]
				differences[j] = differences[j+1]
				differences[j+1] = temp

				temp = indexes[j]
				indexes[j] = indexes[j+1]
				indexes[j+1] = temp
			}
		}
	}
	return indexes
}
