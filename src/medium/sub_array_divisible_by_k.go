package medium

func SubarraysDivByK(A []int, K int) int {
	result := 0
	for _, val := range A {
		if val%K == 0 {
			result++
		}
	}
	sum := A[0]
	prevSum := sum
	for i := 2; i <= len(A); i++ {
		//fmt.Println(A[0:i])
		sum = prevSum + A[i-1]
		prevSum = sum
		if sum%K == 0 {
			result++
		}

		for j, k := i, 0; j <= len(A)-1; j, k = j+1, k+1 {
			//fmt.Println("removing", A[k], " adding", A[j])
			sum = sum - A[k] + A[j]
			if sum%K == 0 {
				result++
			}
		}

	}
	return result
}
