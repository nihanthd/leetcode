package medium

func Exist(board [][]byte, word string) bool {
	var solution [][]int
	for i := 0; i < len(board); i++ {
		solution = append(solution, make([]int, len(board[i])))
	}
	maxRows := len(board)
	for i := 0; i < maxRows; i++ {
		maxColumns := len(board[i])
		for j := 0; j < maxColumns; j++ {
			index := 0
			if searchInAdjacent(board, solution, i, j, index, maxRows, maxColumns, word) {
				return true
			}
		}
	}
	return false
}

func searchInAdjacent(matrix [][]byte, solution [][]int, row, col, index, maxRows, maxColumns int, input string) bool {

	//if it was pre used or else the input[index] is not equal to the character
	if solution[row][col] != 0 || input[index] != matrix[row][col] {
		return false
	}

	//if at any point we have crossed searching until the last index, that means we have found a solution
	if index == len(input)-1 {
		return true
	}

	solution[row][col] = 1
	index++

	//Now we have eight directions to go to find out if we find a match

	//go →, i.e. increase column in same row
	if col < maxColumns-1 && searchInAdjacent(matrix, solution, row, col+1, index, maxRows, maxColumns, input) {
		return true
	}
	//go ←, i.e. decrease column in same row
	if col > 0 && searchInAdjacent(matrix, solution, row, col-1, index, maxRows, maxColumns, input) {
		return true
	}

	//go ↑, i.e. decrease the row in same column
	if row > 0 && searchInAdjacent(matrix, solution, row-1, col, index, maxRows, maxColumns, input) {
		return true
	}

	//go ↓, i.e. increase the row in same column
	if row < maxRows-1 && searchInAdjacent(matrix, solution, row+1, col, index, maxRows, maxColumns, input) {
		return true
	}

	//If we have not found the solution any where in the eight directions then we have to back track and decrease path

	solution[row][col] = 0
	index--
	return false
}
