package medium

import (
	"bytes"
	"fmt"
	"regexp"
	"strings"
)

func reverseWords(s string) string {
	var output bytes.Buffer
	r, _ := regexp.Compile("\\s+")
	inputWords := r.Split(s, -1)
	for i := len(inputWords) - 1; i >= 0; i-- {
		if len(inputWords[i]) > 0 {
			output.WriteString(inputWords[i] + " ")
		}
	}
	return strings.TrimSpace(output.String())
}

func ReverseWords() {
	input := " the sky is blue "
	output := reverseWords(input)
	fmt.Printf("The reverse of '%s' is '%s'\n", input, output)
}
