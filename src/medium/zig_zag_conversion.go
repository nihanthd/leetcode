package medium

import (
	"bytes"
	"fmt"
)

/*
https://leetcode.com/problems/zigzag-conversion/description/
*/
func convert(s string, numRows int) string {
	var output bytes.Buffer
	rows := make([]string, numRows)
	curRow := 0
	goingDown := false

	for i := 0; i < len(s); i++ {
		temp := rows[curRow]
		var b bytes.Buffer
		b.WriteString(temp)
		b.WriteString(string(s[i]))
		rows[curRow] = b.String()
		if curRow == 0 || curRow == numRows-1 {
			goingDown = !goingDown
		}
		if goingDown {
			curRow += 1
		} else {
			curRow -= 1
		}
	}

	for _, val := range rows {
		output.WriteString(val)
	}
	return output.String()
}

func ZigZagConvert() {
	input := "PAYPALISHIRING"
	output := convert(input, 4)
	fmt.Println(output)
}
