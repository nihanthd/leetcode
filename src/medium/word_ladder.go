package medium

/*
https://leetcode.com/problems/word-ladder/
*/
func ladderLength(beginWord string, endWord string, wordList []string) int {
	//begin word would be the root
	root := &GraphNode{Word: beginWord, adjacencyList: []*GraphNode{}}
	//now from root build the required graph with each node at 1 distance and this distance means that there is 1 transformation at any character
	buildGraph(root, wordList)
	queue := []*GraphNode{root}
	for len(queue) != 0 {
		node := queue[0]
		queue = queue[1:]
		//if we have already visited this node from some other path then skip visiting again
		//since it would be a bidirectional graph.
		if node.visited {
			continue
		}
		//mark the node as visited
		node.visited = true
		//if we have come across the word
		if node.Word == endWord {
			return node.depth + 1
		}
		//for each node in the adjacency list of the current node, calculate the depth
		for _, p := range node.adjacencyList {
			//we calculate the depth only if the node is not equal to the root and its depth is equal to zero.
			//otherwise its depth is already calculated.
			if p != root && p.depth == 0 {
				p.depth = node.depth + 1
				queue = append(queue, p)
			}
		}
	}
	return 0
}

func buildGraph(root *GraphNode, wordList []string) {
	//From root for each character see what words in the list can become an adjacent node
	// even before that let us convert all the words in the wordList to node
	nodes := make([]*GraphNode, len(wordList))
	for i := range wordList {
		nodes[i] = &GraphNode{wordList[i], []*GraphNode{}, 0, false}
	}
	alreadyBuilt := map[*GraphNode]bool{root: true}
	buildAdjacencyList(root, nodes, alreadyBuilt)
}

func buildAdjacencyList(current *GraphNode, nodes []*GraphNode, alreadyBuilt map[*GraphNode]bool) {
	//now we have to construct the adjacency list for the current node
	for _, node := range nodes {
		if len(current.Word) == 0 || len(node.Word) == 0 || len(current.Word) != len(node.Word) {
			continue
		}

		difference := 0
		for index := range current.Word {
			if current.Word[index] != node.Word[index] {
				difference++
			}
		}
		if difference > 1 {
			continue
		}
		current.adjacencyList = append(current.adjacencyList, node)
	}
	//for each node in the adjacency list we need to find the adjacency list
	for _, node := range current.adjacencyList {
		if alreadyBuilt[node] {
			continue
		}
		alreadyBuilt[node] = true
		buildAdjacencyList(node, nodes, alreadyBuilt)
	}
}

type GraphNode struct {
	Word          string
	adjacencyList []*GraphNode
	depth         int
	visited       bool
}
