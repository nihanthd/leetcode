package medium

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGroupAnagrams(t *testing.T) {
	input := []string{"eat", "tea", "tan", "ate", "nat", "bat"}
	expectedOutput := [][]string{
		{"bat"}, {"eat", "tea", "ate"}, {"tan", "nat"},
	}
	actualOutput := groupAnagrams(input)
	assert.Equal(t, len(expectedOutput), len(actualOutput), "")
}
