package medium

import (
	"bytes"
	"fmt"
)

func letterCombinations(digits string) []string {
	phone := map[byte][]string{
		'1': {},
		'2': {"a", "b", "c"},
		'3': {"d", "e", "f"},
		'4': {"g", "h", "i"},
		'5': {"j", "k", "l"},
		'6': {"m", "n", "o"},
		'7': {"p", "q", "r", "s"},
		'8': {"t", "u", "v"},
		'9': {"w", "x", "y", "z"},
	}

	var output []string

	for i := 0; i <= len(digits)-1; i++ {
		tempOutput := []string{}
		if len(output) == 0 {
			output = phone[digits[i]]
		} else {
			temp := phone[digits[i]]
			if len(temp) > 0 {
				for j := 0; j < len(output); j++ {
					for k := 0; k < len(temp); k++ {
						var b bytes.Buffer
						b.WriteString(output[j])
						b.WriteString(temp[k])
						tempOutput = append(tempOutput, b.String())
					}
				}
				output = tempOutput
			}
		}
	}

	return output
}

func LetterCombinations() {
	output := letterCombinations("2987")
	fmt.Println(len(output))
	for _, val := range output {
		fmt.Println(val)
	}
}
