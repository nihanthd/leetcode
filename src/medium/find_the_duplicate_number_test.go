package medium

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindDuplicate(t *testing.T) {
	input := []int{1, 4, 6, 6, 6, 2, 3}
	output := findDuplicate(input)
	expectedOutput := 6
	assert.Equal(t, expectedOutput, output, "")

}
