package medium

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProductExceptSelf(t *testing.T) {
	input := []int{1, 2, 3, 4}
	expectedOutput := []int{24, 12, 8, 6}
	output := ProductExceptSelf(input)
	assert.Equal(t, expectedOutput, output)
}

func TestNumberOfIslands(t *testing.T) {
	grid := [][]byte{
		{'1', '1', '1', '1', '0'},
		{'1', '1', '0', '1', '0'},
		{'1', '1', '0', '0', '0'},
		{'0', '0', '0', '0', '0'},
	}
	output := numIslands(grid)
	expectedOutput := 1
	assert.Equal(t, expectedOutput, output)

	grid = [][]byte{
		{'1', '1', '0', '0', '0'},
		{'1', '1', '0', '0', '0'},
		{'0', '0', '1', '0', '0'},
		{'0', '0', '0', '1', '1'},
	}

	output = numIslands(grid)
	expectedOutput = 3
	assert.Equal(t, expectedOutput, output)

	grid = [][]byte{
		{'1', '1', '1'},
		{'0', '1', '0'},
		{'1', '1', '1'},
	}

	output = numIslands(grid)
	expectedOutput = 1
	assert.Equal(t, expectedOutput, output)
}

func TestBinaryTreePruning(t *testing.T) {
	root := &TreeNode{
		Val:  1,
		Left: nil,
		Right: &TreeNode{
			Val: 0,
			Left: &TreeNode{
				Val:   0,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   1,
				Left:  nil,
				Right: nil,
			},
		},
	}
	outputRoot := pruneTree(root)
	expectedRoot := &TreeNode{
		Val:  1,
		Left: nil,
		Right: &TreeNode{
			Val:  0,
			Left: nil,
			Right: &TreeNode{
				Val:   1,
				Left:  nil,
				Right: nil,
			},
		},
	}
	assert.Equal(t, expectedRoot, outputRoot)
	assert.Nil(t, outputRoot.Right.Left)

	root = &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val: 0,
			Left: &TreeNode{
				Val:   0,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   0,
				Left:  nil,
				Right: nil,
			},
		},
		Right: &TreeNode{
			Val: 1,
			Left: &TreeNode{
				Val:   0,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   1,
				Left:  nil,
				Right: nil,
			},
		},
	}

	outputRoot = pruneTree(root)
	expectedRoot = &TreeNode{
		Val:  1,
		Left: nil,
		Right: &TreeNode{
			Val:  1,
			Left: nil,
			Right: &TreeNode{
				Val:   1,
				Left:  nil,
				Right: nil,
			},
		},
	}
	assert.Equal(t, expectedRoot, outputRoot)
	assert.Nil(t, outputRoot.Right.Left)
	assert.Nil(t, outputRoot.Left)
}

func TestReorderList(t *testing.T) {
	head := &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 2,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val: 4,
					Next: &ListNode{
						Val:  5,
						Next: nil,
					},
				},
			},
		},
	}

	reorderList(head)
	head.Print()
}

func TestWordLadder(t *testing.T) {
	beginWord, endWord := "hit", "cog"
	wordList := []string{"hot", "dot", "dog", "lot", "log", "cog"}
	length := ladderLength(beginWord, endWord, wordList)
	expectedLength := 5

	assert.Equal(t, expectedLength, length)

	beginWord = "hit"
	endWord = "cog"
	wordList = []string{"hot", "dot", "dog", "lot", "log"}
	length = ladderLength(beginWord, endWord, wordList)
	expectedLength = 0

	assert.Equal(t, expectedLength, length)
}

func TestAirbnb(t *testing.T) {
	prices := []float32{0.70, 2.80, 4.90}
	target := 8
	output := roundPricesToMatchTarget(prices, int32(target))
	expected := []int32{0, 3, 5}
	assert.Equal(t, expected, output)
}

func TestLetterCombinations(t *testing.T) {
	input := "23"
	output := letterCombinations(input)
	expectedOutput := []string{"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"}
	assert.Equal(t, expectedOutput, output)
}

func TestPopulateNext(t *testing.T) {
	root := &TreeLinkNode{
		Val: 1,
		Left: &TreeLinkNode{
			Val: 2,
			Left: &TreeLinkNode{
				Val:   4,
				Left:  nil,
				Right: nil,
				Next:  nil,
			},
			Right: &TreeLinkNode{
				Val:   5,
				Left:  nil,
				Right: nil,
				Next:  nil,
			},
			Next: nil,
		},
		Right: &TreeLinkNode{
			Val: 3,
			Left: &TreeLinkNode{
				Val:   6,
				Left:  nil,
				Right: nil,
				Next:  nil,
			},
			Right: &TreeLinkNode{
				Val:   7,
				Left:  nil,
				Right: nil,
				Next:  nil,
			},
			Next: nil,
		},
		Next: nil,
	}
	assert.NotPanics(t, func() { connect(root) })

	root = &TreeLinkNode{
		Val: 1,
		Left: &TreeLinkNode{
			Val: 2,
			Left: &TreeLinkNode{
				Val:   4,
				Left:  nil,
				Right: nil,
				Next:  nil,
			},
			Right: &TreeLinkNode{
				Val:   5,
				Left:  nil,
				Right: nil,
				Next:  nil,
			},
			Next: nil,
		},
		Right: &TreeLinkNode{
			Val:  3,
			Left: nil,
			Right: &TreeLinkNode{
				Val:   7,
				Left:  nil,
				Right: nil,
				Next:  nil,
			},
			Next: nil,
		},
		Next: nil,
	}

	assert.NotPanics(t, func() { connect2(root) })
	root = &TreeLinkNode{}
}

func TestXMain(t *testing.T) {
	xmain()
}

func TestIntToRoman(t *testing.T) {
	output := intToRoman(1949)
	expected := "MCMXLIX"
	assert.Equal(t, expected, output)

	output = intToRoman(2944)
	expected = "MMCMXLIV"
	assert.Equal(t, expected, output)

	output = intToRoman(1866)
	expected = "MDCCCLXVI"
	assert.Equal(t, expected, output)

	output = intToRoman(1493)
	expected = "MCDXCIII"
	assert.Equal(t, expected, output)

	output = intToRoman(-1)
	expected = ""
	assert.Equal(t, expected, output)
}

func TestSearchMatrix(t *testing.T) {
	matrix := [][]int{{1, 3, 5, 7},
		{10, 11, 16, 20},
		{23, 30, 34, 50},
	}
	actual := SearchMatrix(matrix, 3)
	assert.True(t, actual)

	actual = SearchMatrix(matrix, 13)
	assert.False(t, actual)
}

func TestCoinChange(t *testing.T) {
	coins := []int{1, 2, 5}
	amount := 11
	actual := coinChange(coins, amount)
	expected := 3
	assert.Equal(t, expected, actual)

	coins = []int{2}
	amount = 3
	actual = coinChange(coins, amount)
	expected = -1
	assert.Equal(t, expected, actual)
}

func TestCanReorderDoubled(t *testing.T) {
	A := []int{3, 1, 3, 6}
	assert.False(t, CanReorderDoubled(A))

	A = []int{4, -2, 2, -4}
	assert.True(t, CanReorderDoubled(A))

	A = []int{1, 2, 4, 16, 8, 4}
	assert.False(t, CanReorderDoubled(A))

	A = []int{2, 1, 2, 6}
	assert.False(t, CanReorderDoubled(A))

	A = []int{1, 2, 1, -8, 8, -4, 4, -4, 2, -2}
	assert.True(t, CanReorderDoubled(A))
}

//func TestPourWater(t *testing.T) {
//	heights := []int{2, 1, 1, 2, 1, 2, 2}
//	V, K := 4, 3
//	actual := PourWater(heights, V, K)
//	expected := []int{2, 2, 2, 3, 2, 2, 2}
//	assert.Equal(t, expected, actual)
//}

func TestIsCompletenessOfBinaryTree(t *testing.T) {
	root := &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val: 2,
			Left: &TreeNode{
				Val: 4,
			},
			Right: &TreeNode{
				Val: 5,
			},
		},
		Right: &TreeNode{
			Val: 3,
			Left: &TreeNode{
				Val: 6,
			},
		},
	}
	assert.True(t, isCompleteTree(root))

	root = &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val: 2,
			Left: &TreeNode{
				Val: 4,
			},
			Right: &TreeNode{
				Val: 5,
			},
		},
		Right: &TreeNode{
			Val: 3,
			Right: &TreeNode{
				Val: 7,
			},
		},
	}
	assert.False(t, isCompleteTree(root))

	root = &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val: 2,
			Left: &TreeNode{
				Val: 4,
				Left: &TreeNode{
					Val: 8,
					Left: &TreeNode{
						Val: 15,
					},
				},
				Right: &TreeNode{
					Val: 9,
				},
			},
			Right: &TreeNode{
				Val: 5,
				Left: &TreeNode{
					Val: 10,
				},
				Right: &TreeNode{
					Val: 11,
				},
			},
		},
		Right: &TreeNode{
			Val: 3,
			Left: &TreeNode{
				Val: 6,
				Left: &TreeNode{
					Val: 12,
				},
				Right: &TreeNode{
					Val: 13,
				},
			},
			Right: &TreeNode{
				Val: 7,
			},
		},
	}
	assert.False(t, isCompleteTree(root))

}

func TestPrisonAfterNDays(t *testing.T) {
	cells := []int{1, 0, 0, 1, 0, 0, 1, 0}
	expected := []int{0, 0, 1, 1, 1, 1, 1, 0}
	N := 1000000000
	actual := prisonAfterNDays(cells, N)
	assert.Equal(t, expected, actual)

	cells = []int{0, 1, 0, 1, 1, 0, 0, 1}
	expected = []int{0, 0, 1, 1, 0, 0, 0, 0}
	N = 7
	actual = prisonAfterNDays(cells, N)
	assert.Equal(t, expected, actual)

	cells = []int{1, 0, 0, 1, 0, 0, 0, 1}
	expected = []int{0, 1, 1, 0, 1, 1, 1, 0}
	N = 826
	actual = prisonAfterNDays(cells, N)
	assert.Equal(t, expected, actual)
}

func TestCanJump(t *testing.T) {
	nums := []int{2, 3, 1, 1, 4}
	assert.True(t, CanJump(nums))

	nums = []int{3, 2, 1, 0, 4}
	assert.False(t, CanJump(nums))
}

func TestPancakeSort(t *testing.T) {
	A := []int{3, 2, 4, 1}
	fmt.Println(PancakeSort(A))
	A = []int{1, 2, 3}
	fmt.Println(PancakeSort(A))

	sample := []int{3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 4, 3, 2, 4, 3}
	X := []int{3, 2, 4, 1}
	for _, val := range sample {
		X = Flip(X, val)
	}
	fmt.Println(X)
}

func TestSimplifyPath(t *testing.T) {
	path := "/home/"
	actual := SimplifyPath(path)
	expected := "/home"
	assert.Equal(t, expected, actual)

	path = "/a/./b/../../c/"
	actual = SimplifyPath(path)
	expected = "/c"
	assert.Equal(t, expected, actual)

	path = "/a/../../b/../c//.//"
	actual = SimplifyPath(path)
	expected = "/c"
	assert.Equal(t, expected, actual)

	path = "/a//b////c/d//././/.."
	actual = SimplifyPath(path)
	expected = "/a/b/c"
	assert.Equal(t, expected, actual)

	path = "/home//foo/"
	actual = SimplifyPath(path)
	expected = "/home/foo"
	assert.Equal(t, expected, actual)

	path = "/"
	actual = SimplifyPath(path)
	expected = "/"
	assert.Equal(t, expected, actual)
}

func TestSubarraysDivByK(t *testing.T) {
	A := []int{4, 5, 0, -2, -3, 1}
	K := 5

	actual := SubarraysDivByK(A, K)
	expected := 7
	assert.Equal(t, expected, actual)
}

func TestExist(t *testing.T) {
	board := [][]byte{{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}}
	word := "ABCCED"
	assert.True(t, Exist(board, word))

	word = "SEE"
	assert.True(t, Exist(board, word))

	word = "ABCB"
	assert.False(t, Exist(board, word))

	board = [][]byte{{'a'}}
	word = "a"
	assert.True(t, Exist(board, word))
}

func TestTotalFruit(t *testing.T) {
	tree := []int{1, 2, 1}
	actual := TotalFruit(tree)
	expected := 3
	assert.Equal(t, expected, actual)

	tree = []int{3, 3, 3, 1, 2, 1, 1, 2, 3, 3, 4}
	actual = TotalFruit(tree)
	expected = 5
	assert.Equal(t, expected, actual)
}
