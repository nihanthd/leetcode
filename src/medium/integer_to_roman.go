package medium

import (
	"bytes"
)

/*

 */
func intToRoman(num int) string {
	buf := &bytes.Buffer{}

	if num <= 0 {
		return ""
	}

	for num > 0 {
		if num >= 1000 {
			times(buf, num/1000, 'M')
			num = num % 1000
		} else if num >= 500 {
			if num < 900 {
				times(buf, num/500, 'D')
				num = num % 500
			} else {
				appendDigits(buf, 'C', 'M')
				num = num % 100
			}
		} else if num >= 100 {
			if num < 400 {
				times(buf, num/100, 'C')
				num = num % 100
			} else {
				appendDigits(buf, 'C', 'D')
				num = num % 100
			}
		} else if num >= 50 {
			if num < 90 {
				times(buf, num/50, 'L')
				num = num % 50
			} else {
				appendDigits(buf, 'X', 'C')
				num = num % 10
			}

		} else if num >= 10 {
			if num < 40 {
				times(buf, num/10, 'X')
				num = num % 10
			} else {
				appendDigits(buf, 'X', 'L')
				num = num % 10
			}

		} else if num >= 5 {
			if num < 9 {
				times(buf, num/5, 'V')
				num = num % 5
			} else {
				appendDigits(buf, 'I', 'X')
				num = 0
			}

		} else if num >= 1 {
			if num < 4 {
				times(buf, num, 'I')
				num = 0
			} else {
				appendDigits(buf, 'I', 'V')
				num = 0
			}
		}
	}

	return buf.String()
}

func appendDigits(buf *bytes.Buffer, firstChar, secondChar byte) {
	buf.WriteByte(firstChar)
	buf.WriteByte(secondChar)
}

func times(buf *bytes.Buffer, n int, char byte) {
	for n > 0 {
		buf.WriteByte(char)
		n--
	}
}
