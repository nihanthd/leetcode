package medium

func removeNthFromEnd(head *ListNode, n int) *ListNode {
	l1, l2 := head, head
	i := 0
	for l1 != nil {
		l1 = l1.Next
		if i > n {
			l2 = l2.Next
		}
		i++
	}
	if i == n {
		return l2.Next
	}

	l2.Next = l2.Next.Next

	return head
}

func RemoveNthFromEnd() {
	head := &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 2,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val: 4,
					Next: &ListNode{
						Val:  5,
						Next: nil,
					},
				},
			},
		},
	}

	//head := &ListNode{
	//	Val:  1,
	//	Next: nil,
	//}
	head = removeNthFromEnd(head, 5)
	head.Print()
}
