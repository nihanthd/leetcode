package medium

import (
	"math"
)

func isCompleteTree(root *TreeNode) bool {
	var queue []*TreeNode
	h := 0
	queue = append(queue, root)
	for len(queue) != 0 {
		var tempQueue, nilQueue []*TreeNode

		for _, node := range queue {
			if node != nil {
				if node.Left != nil {
					tempQueue = append(tempQueue, node.Left)
				}
				if node.Right != nil {
					tempQueue = append(tempQueue, node.Right)
				}
				nilQueue = append(nilQueue, node.Left)
				nilQueue = append(nilQueue, node.Right)
			}
		}
		numberOfNodes := int(math.Pow(float64(2), float64(h)))
		isThereAnEmptyInBetween := false
		count := 0
		for _, node := range queue {
			if node == nil {
				isThereAnEmptyInBetween = true
			} else {
				count++
			}
			if isThereAnEmptyInBetween && node != nil {
				return false
			}
		}

		if len(tempQueue) > 0 && count != numberOfNodes {
			return false
		}

		queue = nilQueue
		h++
	}
	return true
}
