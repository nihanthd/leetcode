package medium

/*
https://leetcode.com/problems/populating-next-right-pointers-in-each-node/description/
*/
func connect(root *TreeLinkNode) {
	//if root == nil return
	if root == nil {
		return
	}

	//populate next for children
	if root.Left != nil && root.Right != nil {
		//connect left and right
		root.Left.Next = root.Right
	}
	//populate next across siblings
	if root.Right != nil && root.Next != nil && root.Next.Left != nil {
		root.Right.Next = root.Next.Left
	}

	//populate children's next
	connect(root.Left)
	connect(root.Right)
}

/*
https://leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/
*/

func connect2(root *TreeLinkNode) {
	//if root == nil return
	if root == nil {
		return
	}
	temphead := root.Next
	//populate next for children
	for temphead != nil {
		if root.Left != nil && root.Right != nil {
			//connect left and right
			root.Left.Next = root.Right
		}

		//what if right was empty and next.left is not empty
		if root.Left != nil && root.Right == nil && root.Left.Next == nil && temphead.Left != nil {
			root.Left.Next = temphead.Left
		}

		//what if left was nil in temphead.Left
		if root.Left != nil && root.Right == nil && root.Left.Next == nil && temphead.Right != nil {
			root.Left.Next = temphead.Right
		}

		//populate next across siblings
		if root.Right != nil && temphead != nil && temphead.Left != nil {
			root.Right.Next = temphead.Left
		}

		if root.Right != nil && root.Right.Next == nil && temphead != nil && temphead.Right != nil {
			root.Right.Next = temphead.Right
		}
		temphead = temphead.Next
	}

	//populate children's next
	connect2(root.Left)
	connect2(root.Right)
}
