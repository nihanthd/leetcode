package medium

import "fmt"

/*
https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/description/
*/
func lowestCommonAncestor(root, p, q *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}

	if (root.Val == p.Val) || (root.Val == q.Val) {
		return root
	}

	left := lowestCommonAncestor(root.Left, p, q)
	right := lowestCommonAncestor(root.Right, p, q)

	if left != nil && right != nil {
		return root
	}

	if left != nil {
		return left
	} else {
		return right
	}
}

func LowestCommonAncestor() {
	p := &TreeNode{
		Val:   7,
		Left:  nil,
		Right: nil,
	}

	q := &TreeNode{
		Val:   4,
		Left:  nil,
		Right: nil,
	}

	root := &TreeNode{
		Val: 3,
		Left: &TreeNode{
			Val: 5,
			Left: &TreeNode{
				Val:   6,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val: 2,
				Left: &TreeNode{
					Val:   7,
					Left:  nil,
					Right: nil,
				},
				Right: &TreeNode{
					Val:   4,
					Left:  nil,
					Right: nil,
				},
			},
		},
		Right: &TreeNode{
			Val: 1,
			Left: &TreeNode{
				Val:   0,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   8,
				Left:  nil,
				Right: nil,
			},
		},
	}

	lca := lowestCommonAncestor(root, p, q)
	fmt.Println(lca)
}
