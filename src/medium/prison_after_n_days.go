package medium

import "fmt"

func prisonAfterNDays(cells []int, N int) []int {
	N = N % 14
	if N == 0 {
		N = 14
	}
	fmt.Println("0\t->\t", cells)
	for i := 1; i <= N; i++ {
		tempCells := make([]int, len(cells))
		for j := 1; j < len(cells)-1; j++ {
			leftCell := cells[j-1]
			rightCell := cells[j+1]

			if leftCell == rightCell {
				tempCells[j] = 1
			} else {
				tempCells[j] = 0
			}
		}
		cells = tempCells
		fmt.Println(i, "\t->\t", cells)
	}
	return cells
}
