package medium

func findBottomLeftValue(root *TreeNode) int {
	var leftMostValue int
	queue1 := []*TreeNode{root}
	for len(queue1) != 0 {
		leftMostValue = queue1[0].Val
		var queue2 []*TreeNode
		for _, node := range queue1 {
			if node.Left != nil {
				queue2 = append(queue2, node.Left)
			}
			if node.Right != nil {
				queue2 = append(queue2, node.Right)
			}
		}
		queue1 = queue2
	}
	return leftMostValue
}
