package medium

import "fmt"

/*
https://leetcode.com/problems/container-with-most-water/description/
*/

func maxArea(height []int) int {
	maxWater, left, right := 0, 0, len(height)-1
	for left < right {
		maxWater = max(maxWater, min(height[left], height[right])*(right-left))
		if height[left] < height[right] {
			left++
		} else {
			right--
		}
	}
	return maxWater
}

func ContainerWithMostWater() {
	height := []int{1, 8, 6, 2, 5, 4, 8, 3, 7}
	maxWater := maxArea(height)
	fmt.Printf("The max water that can fit is : %d\n", maxWater)
}
