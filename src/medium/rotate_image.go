package medium

import (
	"fmt"
)

func rotate(matrix [][]int) {
	size := len(matrix)
	for x := 0; size >= 2; size, x = size-1, x+1 {
		//corresponding coordinates are (i,a) (j,b) (k,c) (l,d)
		i, j, k, l := x, x, size-1, size-1
		a, b, c, d := x, size-1, size-1, x
		for ; a < size-1; a++ {
			temp := matrix[i][a]
			matrix[i][a] = matrix[l][d]
			matrix[l][d] = matrix[k][c]
			matrix[k][c] = matrix[j][b]
			matrix[j][b] = temp
			//Increment & decrement accordingly
			j++
			c--
			l--
		}
	}
	printMatrix(matrix)
}

func printMatrix(matrix [][]int) {
	for _, arr := range matrix {
		fmt.Println(arr)
	}
}

func RotateImage() {
	//matrix := [][]int{
	//	{5, 1, 9, 11},
	//	{2, 4, 8, 10},
	//	{13, 3, 6, 7},
	//	{15, 14, 12, 16},
	//}
	//matrix := [][]int{
	//	{1, 2, 3},
	//	{4, 5, 6},
	//	{7, 8, 9},
	//}
	matrix := [][]int{
		{1, 2},
		{3, 4},
	}
	rotate(matrix)
}
