package medium

import (
	"fmt"
)

/*
https://leetcode.com/problems/validate-binary-search-tree/description/
*/

func isValidBST(root *TreeNode) bool {
	if root == nil {
		return true
	}

	nums := traverseBST(root)
	for i := 0; i < len(nums)-1; i++ {
		if nums[i] > nums[i+1] {
			return false
		}
	}
	return true
}

func traverseBST(root *TreeNode) []int {
	if root == nil {
		return []int{}
	}
	var nums []int
	left := traverseBST(root.Left)
	nums = append(nums, left...)
	nums = append(nums, root.Val)
	right := traverseBST(root.Right)
	nums = append(nums, right...)
	return nums
}

func IsValidBST() {
	//root := &TreeNode{
	//	Val: 5,
	//	Left: &TreeNode{
	//		Val: 3,
	//		Left: &TreeNode{
	//			Val:   2,
	//			Left:  nil,
	//			Right: nil,
	//		},
	//		Right: &TreeNode{
	//			Val:   4,
	//			Left:  nil,
	//			Right: nil,
	//		},
	//	},
	//	Right: &TreeNode{
	//		Val: 8,
	//		Left: &TreeNode{
	//			Val:   6,
	//			Left:  nil,
	//			Right: nil,
	//		},
	//		Right: &TreeNode{
	//			Val:   9,
	//			Left:  nil,
	//			Right: nil,
	//		},
	//	},
	//}

	root := &TreeNode{
		Val: 5,
		Left: &TreeNode{
			Val:   1,
			Left:  nil,
			Right: nil,
		},
		Right: &TreeNode{
			Val: 4,
			Left: &TreeNode{
				Val:   3,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   6,
				Left:  nil,
				Right: nil,
			},
		},
	}
	isValid := isValidBST(root)
	fmt.Printf("is the given tree is a valid BST? %t\n", isValid)
}
