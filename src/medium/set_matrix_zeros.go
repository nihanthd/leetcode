package medium

/*
https://leetcode.com/problems/set-matrix-zeroes/description/
*/

func setZeroes(matrix [][]int) {
	columns, rows, col0 := len(matrix[0])-1, len(matrix)-1, 1
	for i := 0; i <= rows; i++ {
		if matrix[i][0] == 0 {
			col0 = 0
		}
		for j := 1; j <= columns; j++ {
			if matrix[i][j] == 0 {
				matrix[i][0] = 0
				matrix[0][j] = 0
			}
		}
	}

	for i := rows; i >= 0; i-- {
		for j := columns; j >= 1; j-- {
			if matrix[0][j] == 0 || matrix[i][0] == 0 {
				matrix[i][j] = 0
			}
		}
		if col0 == 0 {
			matrix[i][0] = 0
		}
	}
}

func SetZeros() {
	//input := [][]int{
	//	{0, 1, 2, 0},
	//	{3, 4, 5, 2},
	//	{1, 3, 1, 5},
	//}
	input := [][]int{
		{1, 1, 1},
		{1, 0, 1},
		{1, 1, 1},
	}
	setZeroes(input)
	printMatrix(input)
}
