package medium

import (
	"fmt"
	"strconv"
	"strings"
)

func compareVersion(version1 string, version2 string) int {
	var output int
	version1Array := strings.Split(version1, ".")
	version2Array := strings.Split(version2, ".")
	var loopBreak bool
	for i, j := 0, 0; i < len(version1Array) || j < len(version2Array); i, j = i+1, j+1 {
		var version1Element, version2Element int
		if i >= len(version1Array) {
			version1Element = 0
		} else {
			version1Element, _ = strconv.Atoi(version1Array[i])
		}
		if j >= len(version2Array) {
			version2Element = 0
		} else {
			version2Element, _ = strconv.Atoi(version2Array[j])
		}

		switch true {
		case version1Element > version2Element:
			output = 1
			loopBreak = true
		case version2Element > version1Element:
			output = -1
			loopBreak = true
		}
		if loopBreak {
			break
		}
	}
	return output
}

func CompareVersion() {
	version1 := "01"
	version2 := "1"
	output := compareVersion(version1, version2)
	fmt.Println("Comparing version1 ", version1, " with version2 ", version2, " resulted in ", output)
}
