package medium

import "strings"

/*
https://leetcode.com/problems/simplify-path/
*/

func SimplifyPath(path string) string {
	stack := make([]string, 0)
	dirs := strings.Split(path, "/")
	for _, dir := range dirs {
		if len(dir) > 0 {
			if dir == ".." {
				if len(stack) > 0 {
					stack = stack[0 : len(stack)-1]
				}
				continue
			}
			if dir != "." {
				stack = append(stack, dir)
			}
		}
	}

	final := ""

	for _, dir := range stack {
		final = final + "/" + dir
	}
	if len(final) == 0 {
		return "/"
	}
	return final
}
