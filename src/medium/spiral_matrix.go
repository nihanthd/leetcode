package medium

import "fmt"

func spiralOrder(matrix [][]int) []int {
	var output []int
	if len(matrix) == 0 {
		return output
	}
	left, right, top, bottom := 0, len(matrix[0])-1, 0, len(matrix)-1

	for left <= right && top <= bottom {

		//left to right with top
		for i := left; i <= right; i++ {
			output = append(output, matrix[top][i])
		}

		//top+1 to bottom with right
		for i := top + 1; i <= bottom; i++ {
			output = append(output, matrix[i][right])
		}

		if left < right && top < bottom {
			//from right-1 to left with bottom
			for i := right - 1; i >= left; i-- {
				output = append(output, matrix[bottom][i])
			}
			//from bottom-1 to top with left
			for i := bottom - 1; i > top; i-- {
				output = append(output, matrix[i][left])
			}
		}
		top++
		left++
		bottom--
		right--
	}

	return output
}

func SpiralOrder() {
	//input := [][]int{
	//	{1, 2, 3, 4},
	//	{5, 6, 7, 8},
	//	{9, 10, 11, 12},
	//}
	input := [][]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}
	output := spiralOrder(input)
	fmt.Println("The spiral order of the matrix is -> ", output)
}
