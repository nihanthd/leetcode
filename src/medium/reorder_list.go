package medium

func reorderList(head *ListNode) {
	if head == nil {
		return
	}
	//we need a queue and a stack
	//queue to store the first half
	//stack to store the second half
	//pop from stack and remove from queue and append the elements
	var stack, queue []*ListNode

	//find the mid point
	slow, fast := head, head
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	temp := head
	for slow != nil {
		stack = append(stack, slow)
		queue = append(queue, temp)
		slow = slow.Next
		temp = temp.Next
	}
	for i, j := len(stack)-1, 0; i >= 0 && j < len(queue)-1; i, j = i-1, j+1 {
		queue[j].Next = stack[i]
		stack[i].Next = queue[j+1]
	}
	if len(stack) != len(queue) {
		queue[len(queue)-1].Next = nil
	} else {
		stack[0].Next = nil
	}
}
