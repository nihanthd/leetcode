package medium

import (
	"fmt"
	"math"
)

func longestPalindrome(s string) string {
	if len(s) < 1 {
		return s
	}

	start, end := 0, 0

	for i := 0; i < len(s); i++ {
		//if middle is a character
		len1 := expandAroundCenter(s, i, i)
		//If middle is in between two characters
		len2 := expandAroundCenter(s, i, i+1)
		length := int(math.Max(float64(len1), float64(len2)))
		if length > (end - start) {
			//to find the start from i to left
			start = i - ((length - 1) / 2)
			//to find end from i to right
			end = i + (length / 2)
		}
	}
	return s[start : end+1]
}

func expandAroundCenter(s string, left, right int) int {
	L, R := left, right
	//indexes condition should be matching and character on left and character on right should be matching.
	for L >= 0 && R < len(s) && s[L] == s[R] {
		L--
		R++
	}
	return R - L - 1
}

func LongestPallindrome() {
	s := "dabac"
	pallindrome := longestPalindrome(s)
	fmt.Printf("The longest pallindrome in s %s is %s", s, pallindrome)
}
