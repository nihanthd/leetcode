package medium

/*
https://leetcode.com/problems/pour-water
*/

func PourWater(heights []int, V int, K int) []int {
	for V > 0 {
		breakDroplet := false
		for d := -1; d <= 1; d = d + 2 {
			i := K
			best := K
			for i+d >= 0 && i+d < len(heights) && heights[i+d] <= heights[i] {
				if heights[i+d] < heights[i] {
					best = i + d
				}
				i += d
			}
			if heights[best] < heights[K] {
				heights[best]++
				breakDroplet = true
				break
			}
		}
		if !breakDroplet {
			heights[K]++
		}
		V--
	}
	return heights
}
