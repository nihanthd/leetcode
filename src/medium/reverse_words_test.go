package medium

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReverseWordsII(t *testing.T) {
	input := []byte{'t', 'h', 'e', ' ', 's', 'k', 'y', ' ', 'i', 's', ' ', 'b', 'l', 'u', 'e'}
	reverseWordsII(input)
	expectedOutput := []byte{'b', 'l', 'u', 'e', ' ', 'i', 's', ' ', 's', 'k', 'y', ' ', 't', 'h', 'e'}
	assert.Equal(t, expectedOutput, input, "")
}
