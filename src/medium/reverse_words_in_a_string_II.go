package medium

/*
https://leetcode.com/problems/reverse-words-in-a-string-ii/description/
*/

func reverseWordsII(str []byte) {
	//we have reversed the enrite string
	reverse(str, 0, len(str)-1)

	//now reveresing the individual words will give us the desired output
	//how to find the starting and ending of the word
	right := 0
	for right < len(str) {
		left := right
		for right < len(str) && str[right] != ' ' {
			right++
		}
		//the above loop finds the right end of the word. now we need to reverse the word.
		reverse(str, left, right-1)
		//since there is a space after the word
		right++
		//in the second iteration this will assign the left end and move itself to find the right end
	}
}

func reverse(str []byte, start, end int) {
	for i, j := start, end; i <= j; i, j = i+1, j-1 {
		str[i], str[j] = str[j], str[i]
	}
}
