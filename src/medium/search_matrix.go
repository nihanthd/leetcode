package medium

func SearchMatrix(matrix [][]int, target int) bool {
	n := len(matrix[0])
	low, high := 0, len(matrix)*n
	for low < high {
		mid := (low + high) / 2

		midNumber := matrix[mid/n][mid%n]
		if midNumber > target {
			high = mid
		} else if midNumber < target {
			low = mid + 1
		} else {
			return true
		}
	}
	return false
}
