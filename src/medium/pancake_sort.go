package medium

import (
	"fmt"
	"sort"
)

func PancakeSort(A []int) []int {
	var result []int
	if sort.SliceIsSorted(A, func(i, j int) bool { return A[i] <= A[j] }) {
		return result
	}
	for i := len(A); i >= 0; i-- {
		flipped := Flip(A, i)
		//fmt.Println(flipped)
		isSorted, res := pancackeSortRec(flipped, []int{i})
		if isSorted {
			fmt.Println(len(res))
			return res
		}
	}
	return result
}

func pancackeSortRec(flipped []int, res []int) (bool, []int) {
	if len(res) > 10*len(flipped) {
		return false, []int{}
	}
	if sort.SliceIsSorted(flipped, func(i, j int) bool { return flipped[i] <= flipped[j] }) {
		return true, res
	}
	for i := len(flipped); i >= 0; i-- {
		flippedNew := Flip(flipped, i)
		res = append(res, i)
		isSorted, newRes := pancackeSortRec(flippedNew, res)
		if isSorted {
			return isSorted, newRes
		}
	}
	return false, []int{}
}

func Flip(A []int, index int) []int {
	val := make([]int, len(A))
	j := 0
	for i := index - 1; i >= 0; i-- {
		val[j] = A[i]
		j++
	}
	for i := index; i <= len(A)-1; i++ {
		val[j] = A[i]
		j++
	}
	return val
}
