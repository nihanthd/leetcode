package medium

func ProductExceptSelf(nums []int) []int {
	n := len(nums)
	result := make([]int, n)
	temp := 1

	//By multiplying the number with the previous value of num
	// we store what should be the value with which we can multiply
	for i := 0; i < n; i++ {
		result[i] = temp
		temp *= nums[i]
	}
	//Now we come from right to left.
	right := 1
	for i := n - 1; i >= 0; i-- {
		result[i] *= right
		right *= nums[i]
	}

	return result
}
