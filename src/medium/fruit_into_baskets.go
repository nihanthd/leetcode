package medium

/*
https://leetcode.com/problems/fruit-into-baskets/
*/

func TotalFruit(tree []int) int {
	var basket map[int]bool
	maxCount := 0
	count := 0
	for i := 0; i < len(tree); {
		basket = make(map[int]bool)
		move := true
		currentFruit := tree[i]
		for j := i; j < len(tree); j++ {
			fruit := tree[j]
			if currentFruit == fruit && move {
				i++
			} else {
				move = false
			}
			if _, ok := basket[fruit]; ok {
				count++
				continue
			} else {
				if len(basket) <= 1 {
					count++
					basket[fruit] = true
					continue
				} else {
					break
				}
			}
		}
		if count > maxCount {
			maxCount = count
		}
		count = 0
	}
	return maxCount
}
