package medium

import "fmt"
import "github.com/golang-collections/collections/stack"

func inorderTraversal(root *TreeNode) []int {
	s := stack.New()
	fmt.Println("Hi", s)
	return nil
}
func InorderTraversalItrative() {
	root := &TreeNode{
		Val:  1,
		Left: nil,
		Right: &TreeNode{
			Val: 2,
			Left: &TreeNode{
				Val:   3,
				Left:  nil,
				Right: nil,
			},
			Right: nil,
		},
	}
	output := inorderTraversal(root)
	fmt.Println("In order iterative traversal = ", output)
}
