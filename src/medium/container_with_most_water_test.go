package medium

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestContainerWithMostWater(t *testing.T) {
	height := []int{1, 8, 6, 2, 5, 4, 8, 3, 7}
	maxWater := maxArea(height)
	expectedMaxWater := 49
	assert.Equal(t, expectedMaxWater, maxWater, "")
}
