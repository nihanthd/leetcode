package medium

import (
	"fmt"
	"reflect"
)

func detectCycle(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	//Let us start both the pointer's at head
	fastPointer := head
	slowPointer := head
	var loopFound bool
	for fastPointer != nil && fastPointer.Next != nil {
		//Move one of the pointer faster than the other
		fastPointer = fastPointer.Next.Next
		slowPointer = slowPointer.Next
		if reflect.DeepEqual(slowPointer, fastPointer) {
			loopFound = true
			break
		}
	}
	if loopFound {
		//let us start a pointer at head again and now move both at same pace. if there are equal at any point then that is a cycle
		for x := head; true; x, fastPointer = x.Next, fastPointer.Next {
			if reflect.DeepEqual(x, fastPointer) {
				return x
			}
		}
	}
	return nil
}

func LinkedListCycle() {
	y := &ListNode{
		Val:  5,
		Next: nil,
	}
	head := &ListNode{
		Val:  4,
		Next: y,
	}
	head.Next.Next = &ListNode{
		Val: 6,
		Next: &ListNode{
			Val: 7,
			Next: &ListNode{
				Val:  8,
				Next: y,
			},
		},
	}
	loopHead := detectCycle(head)
	fmt.Println("The cycle's head is ", loopHead.Val)
}
