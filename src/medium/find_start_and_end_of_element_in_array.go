package medium

func searchRange(nums []int, target int) []int {
	ranges := []int{-1, -1}
	position := binarySearch(nums, target)
	i, j := position, position
	if position != -1 {
		for i > 0 && nums[i-1] == target {
			i--
		}
		for j < len(nums)-1 && nums[j+1] == target {
			j++
		}
		ranges[0] = i
		ranges[1] = j
	}
	return ranges
}

func binarySearch(nums []int, target int) int {
	position := -1
	if len(nums) == 0 {
		return position
	}
	i, j := 0, len(nums)
	for i <= j {
		mid := (i + j) / 2
		if target > nums[mid] {
			i = mid + 1
		} else if target < nums[mid] {
			j = mid - 1
		} else {
			position = mid
			break
		}
	}
	return position
}

func FindStartAndEndPositionOfElementInSortedArray() {

}
