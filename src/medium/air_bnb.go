package medium

func roundPricesToMatchTarget(prices []float32, target int32) []int32 {
	result, _, _ := calculatePossible(prices, target)
	return result
}

func calculatePossible(prices []float32, target int32) ([]int32, bool, float32) {
	if len(prices) == 0 && target == 0 {
		return nil, true, 0.0
	}

	if len(prices) == 0 && target != 0 {
		return nil, false, 0.0
	}
	//for first value we calculate ceil or floor
	//if we take ceil decrease target
	ceil := ceil(prices[0])
	ceilResult, isCeilValid, ceilRoundingError := calculatePossible(prices[1:], target-ceil)
	ceilError := ceilRoundingError - prices[0] + float32(ceil)

	floor := floor(prices[0])
	floorResult, isFloorValid, floorRoundingError := calculatePossible(prices[1:], target-floor)
	floorError := floorRoundingError + prices[0] - float32(floor)

	if isFloorValid && isCeilValid {
		//
		if floorError > ceilError {
			//pick ceil
			return append([]int32{ceil}, ceilResult...), isCeilValid, ceilError
		} else {
			return append([]int32{floor}, floorResult...), isFloorValid, floorError
		}
	} else if isFloorValid {
		return append([]int32{floor}, floorResult...), isFloorValid, floorError
	} else {
		return append([]int32{ceil}, ceilResult...), isCeilValid, ceilError
	}
}

func ceil(val float32) int32 {
	return int32(val) + 1
}

func floor(val float32) int32 {
	return int32(val)
}
