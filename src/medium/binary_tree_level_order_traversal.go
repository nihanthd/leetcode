package medium

/*
https://leetcode.com/problems/binary-tree-level-order-traversal/description/
*/

func levelOrder(root *TreeNode) [][]int {
	var result [][]int
	if root == nil {
		return result
	}
	var q1 []*TreeNode
	q1 = append(q1, root)
	for len(q1) > 0 {
		var q2 []*TreeNode
		var subResult []int
		for _, node := range q1 {
			subResult = append(subResult, node.Val)
			if node.Left != nil {
				q2 = append(q2, node.Left)
			}
			if node.Right != nil {
				q2 = append(q2, node.Right)
			}
		}
		q1 = q2
		result = append(result, subResult)
	}
	return result
}

func LevelOrderTraversalOfBinaryTree() {
	root := &TreeNode{
		Val: 3,
		Left: &TreeNode{
			Val:   9,
			Left:  nil,
			Right: nil,
		},
		Right: &TreeNode{
			Val: 20,
			Left: &TreeNode{
				Val:   15,
				Left:  nil,
				Right: nil,
			},
			Right: &TreeNode{
				Val:   7,
				Left:  nil,
				Right: nil,
			},
		},
	}
	output := levelOrder(root)
	printMatrix(output)
}
