package medium

import (
	"fmt"
	"sort"
	"strconv"
)

/*
https://leetcode.com/problems/largest-number/description/
*/

func largestNumber(nums []int) string {
	var output string

	if len(nums) == 0 {
		return ""
	}
	sort.SliceStable(nums, func(i, j int) bool {
		x := strconv.FormatInt(int64(nums[i]), 10)
		y := strconv.FormatInt(int64(nums[j]), 10)
		if x+y > y+x {
			return true
		} else {
			return false
		}
	})

	if strconv.FormatInt(int64(nums[0]), 10) == "0" {
		return "0"
	}

	for _, val := range nums {
		output = output + strconv.FormatInt(int64(val), 10)
	}

	return output
}

func GenerateLargestNumber() {
	input := []int{3, 30, 34, 5, 9}
	x := strconv.FormatInt(int64(3), 10)
	y := strconv.FormatInt(int64(30), 10)
	fmt.Println(x + y)
	fmt.Println(y + x)
	output := largestNumber(input)
	fmt.Println("The largest number generated from combining ", input, " is ", output)
}
