package medium

import (
	"sort"
)

func CanReorderDoubled(A []int) bool {
	countMap := make(map[int]int)

	for _, number := range A {
		if val, ok := countMap[number]; ok {
			countMap[number] = val + 1
		} else {
			countMap[number] = 1
		}
	}
	sort.Slice(A, func(i, j int) bool {
		return A[i] < A[j]
	})
	for _, number := range A {
		double := number * 2
		if count, ok := countMap[number]; ok {
			if count >= 1 {
				if count, ok := countMap[double]; ok {
					if count > 1 {
						countMap[double] = count - 1
					} else {
						delete(countMap, double)
					}
					if count, ok := countMap[number]; ok {
						if count > 1 {
							countMap[number] = count - 1
						} else {
							delete(countMap, number)
						}
					}
				}
			}
		}

	}

	if len(countMap) > 0 {
		return false
	}
	return true
}
