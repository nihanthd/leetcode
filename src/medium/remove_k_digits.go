package medium

import (
	"fmt"
)

/*
https://leetcode.com/problems/remove-k-digits/description/
*/

func removeKDigits(num string, k int) string {
	length := len(num)
	if length == k {
		return "0"
	}
	/*
		Find the digits that can be removed
	*/
	var stack []byte
	keep := len(num) - k
	//insert values into stack and pop until the next incoming digit is smaller than the top of stack.
	for i := 0; i < len(num); i++ {
		for len(stack) > 0 && stack[len(stack)-1] > num[i] && k > 0 {
			stack = stack[0 : len(stack)-1]
			k--
		}
		stack = append(stack, num[i])
	}
	// To make sure we only take values from remaining until len(num)-k
	stack = stack[:keep]

	for stack[0] == '0' && len(stack) > 1 {
		stack = stack[1:len(stack)]
	}

	if len(stack) == 0 {
		return "0"
	}

	return string(stack)
}

func RemoveKDigits() {
	input := "10"
	k := 1
	output := removeKDigits(input, k)
	fmt.Printf("The smallest number formed = %s\n", output)
}
