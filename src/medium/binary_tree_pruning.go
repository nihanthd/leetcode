package medium

/*
https://leetcode.com/problems/binary-tree-pruning/
*/

func pruneTree(root *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}
	left := pruneTree(root.Left)
	right := pruneTree(root.Right)
	if left == nil {
		root.Left = nil
	}
	if right == nil {
		root.Right = nil
	}
	if (left != nil) || (right != nil) || root.Val == 1 {
		return root
	}
	return nil
}
