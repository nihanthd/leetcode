package medium

import (
	"fmt"
	"sort"
)

/*This will fail for - []int{44, 10, 29, 12, 49, 41, 23, 5, 17, 26}, 50
func numRescueBoats(people []int, limit int) int {
	var boatsCount int
	length := len(people)
	//boats to know how many boats are assigned.
	boats := make([]int, length)
	for i := 0; i < length; i++ {
		if boats[i] == 0 {
			maxWeight := people[i]
			var indexToNegate int
			//Assign a boat to the person
			boats[i] = 1
			for j := i + 1; j < length; j++ {
				totalWeight := people[i] + people[j]
				if totalWeight <= limit && totalWeight > maxWeight {
					indexToNegate = j
					maxWeight = totalWeight
				}
			}
			if maxWeight > people[i] {
				boats[indexToNegate] = -1
			}
		}
	}
	for i := 0; i < length; i++ {
		if boats[i] > 0 {
			boatsCount++
		}
	}

	return boatsCount
}
*/

func numRescueBoats(people []int, limit int) int {
	var boats int
	//sort people by increasing weight
	sort.Slice(people, func(i, j int) bool {
		return people[i] < people[j]
	})

	for i, j := 0, len(people)-1; i <= j; j-- {
		boats++
		if people[i]+people[j] <= limit {
			i++
		}
	}
	return boats
}

func NumRescueBoats() {
	boats := numRescueBoats([]int{44, 10, 29, 12, 49, 41, 23, 5, 17, 26}, 50)
	fmt.Printf("Minimum boats required to save all people - %d", boats)

}
