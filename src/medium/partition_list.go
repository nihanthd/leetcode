package medium

/*
https://leetcode.com/problems/partition-list/
*/

func Partition(head *ListNode, x int) *ListNode {
	//Find the previous node to the node which is greater than or equal to x
	var prevNode *ListNode
	temp := head
	for temp != nil {
		if temp.Val < x {
			prevNode = temp
			temp = temp.Next
		}
	}
	//We have to place all the nodes before the head node and move the head ahead
	if prevNode == nil {

	}
	return head
}
