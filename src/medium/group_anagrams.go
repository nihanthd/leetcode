package medium

/*
https://leetcode.com/problems/group-anagrams/description/
*/
func groupAnagrams(strs []string) [][]string {
	resultMap := make(map[string][]string)
	var result [][]string
	for _, word := range strs {
		//we have to calculate the string version of 26 characters with indexes as that particular character count
		//that would be the key which we will be using to group the anagrams
		key := make([]int, 26)
		for _, character := range word {
			index := character - 'a'
			key[index] = key[index] + 1
		}
		//convert key to string by appending numbers with some delimiter
		stringKey := convertKeyToString(key, "#")
		if _, ok := resultMap[stringKey]; ok {
			resultMap[stringKey] = append(resultMap[stringKey], word)
		} else {
			resultMap[stringKey] = []string{word}
		}
	}
	for _, value := range resultMap {
		result = append(result, value)
	}
	return result
}

func convertKeyToString(key []int, delimiter string) (output string) {
	for _, number := range key {
		output = output + delimiter + string(number)
	}
	return output
}
