package medium

import (
	"fmt"
)

func xmain() {
	prices, roundError, _ := roundPrices([]float32{0.7, 2.8, 4.9}, 8, 0, []int{}, 0.0, 0.0)
	fmt.Printf("prices=%v roundError=%f\n", prices, roundError)
}

func roundPrices(prices []float32, target int, index int, roundedPrices []int, minRoundError, roundError float32) ([]int, float32, bool) {
	if index >= len(prices) {
		if target == 0 {
			fmt.Printf("target met!! returning %v roundError=%f\n", roundedPrices, roundError)
			return roundedPrices, roundError, true
		} else {
			return []int{}, 0.0, false
		}
	}

	fRound := roundedPrices[:]
	fRound = append(fRound, floor2(prices[index]))
	fRoundError := roundError + (prices[index] - float32(floor2(prices[index])))
	fTarget := target - floor2(prices[index])
	cRound := roundedPrices[:]
	cRound = append(cRound, ceil2(prices[index]))
	cRoundError := roundError + (float32(ceil2(prices[index])) - prices[index])
	cTarget := target - ceil2(prices[index])

	fRoundedPrices, fRoundError, fValid := roundPrices(prices, fTarget, index+1, fRound, minRoundError, fRoundError)
	if fValid && (minRoundError == 0.0 || fRoundError < minRoundError) {
		roundedPrices = fRoundedPrices
		minRoundError = fRoundError
		// fmt.Printf("Set floor roundedPrices=%v fRoundedPrices=%v\n", roundedPrices, fRoundedPrices)
	}

	cRoundedPrices, cRoundError, cValid := roundPrices(prices, cTarget, index+1, cRound, minRoundError, cRoundError)
	if cValid && (minRoundError == 0.0 || cRoundError < minRoundError) {
		roundedPrices = cRoundedPrices
		minRoundError = cRoundError
		// fmt.Printf("Set ceil roundedPrices=%v\n", roundedPrices)
	}

	// fmt.Printf("returning %v roundError=%f\n", roundedPrices, roundError)
	return roundedPrices, minRoundError, fValid || cValid
}

func ceil2(f float32) int {
	return int(f) + 1
}

func floor2(f float32) int {
	return int(f)
}
