package medium

func swapPairs(head *ListNode) *ListNode {
	if head == nil {
		return head
	}
	temp1 := head
	head = head.Next
	var prev *ListNode
	for temp1 != nil {
		temp2 := temp1.Next
		if prev != nil {
			prev.Next = temp2
		}
		temp1.Next = temp2.Next
		temp2.Next = temp1
		prev = temp1
		temp1 = temp1.Next
	}
	return head
}

func SwapPairs() {
	head := &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 2,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val:  4,
					Next: nil,
				},
			},
		},
	}
	head = swapPairs(head)
	head.Print()
}
