package medium

import (
	"fmt"
)

func lengthOfLongestSubstring(s string) int {
	length := 0
	for i := 0; i < len(s); i++ {
		curLength := expandWithNoRepeatingCharacters(s, i)
		if curLength > length {
			length = curLength
		}
	}
	return length
}

func expandWithNoRepeatingCharacters(s string, i int) int {
	left, right := i-1, i+1
	repeats := make(map[byte]int)
	repeats[s[i]] = 1
	var leftBreak, rightBreak bool
	for (left >= 0 || right < len(s)) && !(leftBreak && rightBreak) {
		if left >= 0 && repeats[s[left]] == 0 {
			repeats[s[left]] = 1
			left--
		} else {
			leftBreak = true
		}
		if right < len(s) && repeats[s[right]] == 0 {
			repeats[s[right]] = 1
			right++
		} else {
			rightBreak = true
		}
	}
	return right - left - 1
}

func LengthOfLongestSubstring() {
	length := lengthOfLongestSubstring("abcabcbb")
	fmt.Printf("The longest sub string without repeating characters is %d", length)
}
