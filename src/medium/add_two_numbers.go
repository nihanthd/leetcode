package medium

import "fmt"

// wrong answer

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	var l1Number, l2Number int64
	l1Number = getNumber(l1)
	l2Number = getNumber(l2)
	total := l1Number + l2Number
	root := buildList(total)
	return root
}

func getNumber(l *ListNode) int64 {
	var val int64
	for l != nil {
		val = (val * 10) + int64(l.Val)
		l = l.Next
	}
	return val
}

func buildList(total int64) *ListNode {
	var root, node *ListNode
	for total != 0 {
		rem := total % 10
		total = total / 10
		node = &ListNode{Val: int(rem)}
		node.Next = root
		root = node
	}
	if total == 0 {
		root = &ListNode{0, nil}
	}
	return root
}

func AddTwoNumbers() {
	//l1 := &ListNode{
	//	Val: 7,
	//	Next: &ListNode{
	//		Val: 2,
	//		Next: &ListNode{
	//			Val: 4,
	//			Next: &ListNode{
	//				Val:  3,
	//				Next: nil,
	//			},
	//		},
	//	},
	//}
	//
	//l2 := &ListNode{
	//	Val: 5,
	//	Next: &ListNode{
	//		Val: 6,
	//		Next: &ListNode{
	//			Val:  4,
	//			Next: nil,
	//		},
	//	},
	//}

	l1 := &ListNode{
		0,
		nil,
	}

	l2 := &ListNode{
		0,
		nil,
	}
	l3 := addTwoNumbers(l1, l2)
	fmt.Print(l3)
	for l3 != nil {
		fmt.Println(l3.Val)
		l3 = l3.Next
	}

}
