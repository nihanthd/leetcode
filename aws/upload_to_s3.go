package aws

import (
	"os"
	"strconv"

	"go.uber.org/zap"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type File struct {
	FileName string
}

func UploadToS3(awsBucket, awsKey, awsSecret, path string, logger *zap.Logger) {
	region := "ap-south-1"
	config := &aws.Config{
		Credentials: credentials.NewStaticCredentials(awsKey, awsSecret, ""),
		Region:      &region,
	}
	sess := session.Must(session.NewSession(config))
	uploader := s3manager.NewUploader(sess)
	files := parseDirectory(path, "", logger)
	for _, file := range files {
		f, _ := os.Open(path + file.FileName)
		upParams := &s3manager.UploadInput{
			Bucket: &awsBucket,
			Key:    &(file.FileName),
			Body:   f,
		}

		_, err := uploader.Upload(upParams, func(u *s3manager.Uploader) {
			u.PartSize = 10 * 1024 * 1024 // 10MB part size
			u.LeavePartsOnError = true    // Don't delete the parts if the upload fails.
		})
		if err != nil {
			logger.Error("There was an error", zap.String("error", err.Error()))
		} else {
			logger.Info("Uploaded", zap.String("filename", file.FileName))
			f.Close()
		}
	}
	logger.Info("Length of files = ", zap.String("files", strconv.FormatInt(int64(len(files)), 10)))
}

func parseDirectory(directory, subDirectory string, logger *zap.Logger) []File {
	var file []File
	folder, _ := os.Open(directory + "/" + subDirectory)
	files, _ := folder.Readdir(-1)
	logger.Info("Parsing", zap.String("directory", directory))
	for _, value := range files {
		if value.IsDir() {
			filesInDirectory := parseDirectory(directory, subDirectory+"/"+value.Name(), logger)
			file = append(file, filesInDirectory...)
		} else {
			file = append(file, File{
				FileName: subDirectory + "/" + value.Name(),
			})
		}
	}
	return file
}
