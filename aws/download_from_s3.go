package aws

import (
	"os"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"go.uber.org/zap"
)

func DownloadFromS3(awsBucket, awsKey, awsSecret, region, path string, logger *zap.Logger) {
	createdPaths := make(map[string]bool)
	config := &aws.Config{
		Credentials: credentials.NewStaticCredentials(awsKey, awsSecret, ""),
		Region:      &region,
	}
	sess := session.Must(session.NewSession(config))
	downloader := s3manager.NewDownloader(sess)
	svc := s3.New(session.New(config))
	input := &s3.ListObjectsInput{
		Bucket: aws.String(awsBucket),
		//Prefix: aws.String("/"),
	}
	err := svc.ListObjectsPages(input,
		func(page *s3.ListObjectsOutput, lastPage bool) bool {
			if page != nil {
				//fmt.Println(len(page.))
				//fmt.Println(lastPage)
				for _, content := range page.Contents {
					filePath := *(content.Key)
					dir, _ := filepath.Split(filePath)
					directory := path + "/" + dir
					if _, ok := createdPaths[directory]; !ok {
						err := os.MkdirAll(directory, os.ModeDir)
						if err != nil {
							logger.Error("Error occurred", zap.String("error", err.Error()))
						}
						logger.Info("Created Directory", zap.String("directory", directory))
						createdPaths[directory] = true
					}
					f, err := os.Create(path + "/" + filePath)
					if err != nil {
						logger.Error("failed to create file", zap.String("fileKey", filePath), zap.Error(err))
					}

					// Write the contents of S3 Object to the file
					_, err = downloader.Download(f, &s3.GetObjectInput{
						Bucket: aws.String(awsBucket),
						Key:    content.Key,
					})
					if err != nil {
						logger.Error("failed to download file, %v", zap.Error(err))
					} else {
						logger.Info("Downloaded file", zap.String("file", filePath))
					}

				}
			}
			return !lastPage
		})
	if err != nil {
		logger.Error("Error occurred", zap.String("error", err.Error()))
	}
}
